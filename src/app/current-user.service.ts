import { Injectable, EventEmitter } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

export class Project {

  domain:string;
  name:string;
  description:string;
  hardSkills:{[field: string]: number}[];
  softSkills:{[field: string]: any}[];
  matches: {[field: string]: {[field: string]: any}};
  selected: string[];

  hours:number;
  interns:number;
  weeks:number;
  location:string;

  types:string[];

  accepted:boolean = false;

  role:string;

}

export class Company {

  domain:string;
  projects:{[field: string]: Project};
  
}

export class User {

  uid:string;
  username:string;
  pfpPath:string;
  
  domain:string;
  university:string;
  
  isIntern:boolean;
  mainInfo:{[field: string]: any};
  company:Company;
  
}

@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {

  public isReady = false;
  private isLoading = false;
  private callbacks : (() => void)[] = [];
  public companies: Company[];
  public projects: Project[];

  currentUser: User = null;

  private currentPromise: Promise<User>;

  constructor(private firestore: AngularFirestore) { 
    this.reload();
  }

  async reload() {

    if(this.isLoading) return this.currentPromise;

    this.isLoading = true;
    this.isReady = false;

    this.currentPromise = this.getReloadPromise();
    return this.currentPromise;

  }

  private async getReloadPromise(){
    const uid = localStorage.getItem("uid");
    
    if(uid == null)
      this.currentUser = null;
    else {

      let username:string = "";
      let pfpPath:string = "";
      let domain:string = "";
      let university:string = undefined;
      let isIntern:boolean = true;
      let mainInfo:{[field: string]: any} = {};
      let company:Company = null;

      let user = await this.firestore.doc("users/" + uid).get().toPromise();
      isIntern = user.data()["typeFlag"] != "b";

      const collection = isIntern ? "intern_users/" : "business_users/";

      user = await this.firestore.doc(collection + uid).get().toPromise();
      mainInfo = user.data();
      
      username = mainInfo["username"];
      domain = mainInfo["domain"];
      university = mainInfo["university"];

      const userProfile = await this.firestore.doc(collection + uid + "/profile/main").get().toPromise();
      pfpPath = userProfile.data()["profilePic"];

      if(!isIntern){
        
        company = {
          domain: domain,
          projects: await this.getProjects(domain)
        };

      }

      this.currentUser = {
        uid: uid,
        username: username,
        pfpPath: pfpPath,
        domain: domain,
        university: university,
        isIntern: isIntern,
        mainInfo: mainInfo,
        company: company
      };

      this.companies = [];
      this.projects = [];
      const allComp = await this.firestore.collection("business_page").get().toPromise();
      for(let comp of allComp.docs){
        const newComp = {
          domain: comp.id,
          projects: await this.getProjects(comp.id)
        };

        for(let key in newComp.projects)
          this.projects.push(newComp.projects[key]);

        this.companies.push(newComp);
      }

    }

    for(let callback of this.callbacks)
      callback();
    this.callbacks = [];

    this.isReady = true;
    this.isLoading = false;

    return this.currentUser;
  }

  runWhenReady(callback: () => void){
    if(this.isReady)
      callback();
    else {
      this.reload();
      this.callbacks.push(callback);
    }
  }

  logout(){
    this.currentUser = null;
    this.isReady = true;
  }

  public async reloadProject(name: string){
    const project:Project = await this.getProjectInfo(this.currentUser.company.domain, name);

    this.currentUser.company.projects[name] = project;

    return project;
  }

  public async getProjectInfo(domain: string, name: string){
    const doc = await this.firestore.doc("business_page/" + domain + "/projects/" + name).get().toPromise();

    const data = doc.data();

    name = data["name"];

    const project : Project = {
      domain: domain,
      name: name,
      description: data["description"],
      hardSkills: data["hardSkills"],
      softSkills: data["softSkills"],
      hours: data["hours"],
      interns: data["interns"],
      weeks: data["weeks"],
      location: data["location"],
      types: data["types"],
      matches: data["matches"],
      selected: data["selected"],
      accepted: false,
      role: data["role"]
    };

    return project;
  }

  private async getProjects(domain: string){

    const projects:{[field: string]: Project} = {};
    const projectList = await this.firestore.collection("business_page/" + domain + "/projects").get().toPromise();

    if(projectList != null && projectList != undefined){
      for(let i = 0; i < projectList.docs.length; i++){
        const data = projectList.docs[i].data();
        const name = data["name"];

        const project : Project = {
          domain: domain,
          name: name,
          description: data["description"],
          hardSkills: data["hardSkills"],
          softSkills: data["softSkills"],
          hours: data["hours"],
          interns: data["interns"],
          weeks: data["weeks"],
          location: data["location"],
          types: data["types"],
          matches: data["matches"],
          selected: data["selected"],
          accepted: false,
          role: data["role"]
        };

        projects[name] = project;
      }
    }

    return projects;

  }

}
