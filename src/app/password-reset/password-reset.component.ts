import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {

  onChange(target){

    let valid = false;

    if(target.type === "email"){
      target.value = target.value.slice(0, 64);
      valid = target.value.includes("@") && target.value.endsWith(".edu");
    }
      
    if(valid)
      target.classList.remove('invalid');
    else
      target.classList.add('invalid');

  }

  check(id){
    const el = <HTMLInputElement>document.getElementById(id);
    this.onChange(el);
    return !el.classList.contains("invalid");
  }

  reset(){
    if(this.check("email")){
      const email = (<HTMLInputElement>document.getElementById("email")).value;

      document.getElementById("sent").style.display = "none";
      document.getElementById("error").style.display = "none";
      document.getElementById("sending").style.display = "block";

      this.auth.auth.sendPasswordResetEmail(email).then(() => {
        document.getElementById("sent").style.display = "block";
        document.getElementById("sending").style.display = "none";

        setTimeout(() => {
          if(this.loggedIn)
            this.router.navigate(["dashboard"], {fragment: "account"});
          else
            this.router.navigate(["login"]);
        }, 3000);
      }).catch(err => {
        document.getElementById("sending").style.display = "none";
        document.getElementById("error").style.display = "block";
      });
    }
  }

  constructor(public router:Router, private auth: AngularFireAuth) { }

  ngOnInit() {
  }

  loggedIn:boolean = false;

  ngAfterViewInit(){
    const prEmail = localStorage.getItem("prEmail");
    
    if(prEmail !== null && prEmail !== undefined){
      this.loggedIn = true;
      (<HTMLInputElement> document.getElementById("email")).value = prEmail;
      document.getElementById("head").style.visibility = "hidden";
      document.getElementById("login").style.display = "none";
      document.getElementById("account").style.display = "block";
    }

    localStorage.removeItem("prEmail");
  }

}
