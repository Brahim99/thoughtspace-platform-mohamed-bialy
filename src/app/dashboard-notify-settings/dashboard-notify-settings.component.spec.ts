import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardNotifySettingsComponent } from './dashboard-notify-settings.component';

describe('DashboardNotifySettingsComponent', () => {
  let component: DashboardNotifySettingsComponent;
  let fixture: ComponentFixture<DashboardNotifySettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardNotifySettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardNotifySettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
