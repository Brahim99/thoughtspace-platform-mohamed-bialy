import { Component, OnInit } from '@angular/core';
import {CurrentUserService, User} from '../current-user.service';
import {ActivatedRoute, Router} from "@angular/router";
import {AngularFirestore} from "@angular/fire/firestore";

@Component({
  selector: 'app-dashboard-engage',
  templateUrl: './dashboard-engage.component.html',
  styleUrls: ['./dashboard-engage.component.css']
})
export class DashboardEngageComponent implements OnInit {

  constructor(public currentUserService:CurrentUserService, public router: Router, public firestore: AngularFirestore, public routed: ActivatedRoute) { }

  public postOpen:boolean = false;
  public view:string = "'";

  public allUsers:User[] = null;
  public viewing:User = null;

  async ngOnInit() {

    const frag = this.routed.snapshot.fragment;
    if(frag !== null && frag !== undefined)
      this.view = frag;

    this.routed.url.subscribe(url => {
      if(url !== null && url !== undefined && url.length > 1)
        this.view = url[1].path;
      else
        this.view = "";
    });

    if(!this.currentUserService.isReady){
      await this.currentUserService.reload();
    }

    if(this.allUsers === null){
      const users = await this.firestore.collection("Clariant/results/students").get().toPromise();
      this.allUsers = [];
      for(let doc of users.docs){
        const mainInfo = doc.data();
        const user: User = {
          uid: "",
          username: "",
          pfpPath: "",
          domain: "",
          university: mainInfo["university"],
          isIntern: true,
          mainInfo: mainInfo,
          company: null
        };

        this.allUsers.push(user);
      }
    }

  }

}
