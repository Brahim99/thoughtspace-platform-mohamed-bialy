import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEngageComponent } from './dashboard-engage.component';

describe('DashboardEngageComponent', () => {
  let component: DashboardEngageComponent;
  let fixture: ComponentFixture<DashboardEngageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardEngageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEngageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
