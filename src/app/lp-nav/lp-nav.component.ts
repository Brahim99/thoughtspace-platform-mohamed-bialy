import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lp-nav',
  templateUrl: './lp-nav.component.html',
  styleUrls: ['./lp-nav.component.css']
})
export class LpNavComponent implements OnInit {

  constructor(public router:Router) { }

  ngOnInit() {
  }

}
