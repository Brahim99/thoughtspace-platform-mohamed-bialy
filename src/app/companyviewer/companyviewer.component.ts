import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {ChangeDetectorRef } from '@angular/core'
var firebase = require('firebase/app');

@Component({
  selector: 'app-companyviewer',
  templateUrl: './companyviewer.component.html',
  styleUrls: ['./companyviewer.component.css']
})
export class CompanyviewerComponent implements OnInit {
	projects:any[] = new Array();
	projectsdict = new Object();
	uidtousername = new Object();
	tasksbyuid = new Object();
	enrolledusers = new Array();
	uid:string = ''
	currentuid:string = ''
	feedback:string = ''
  	constructor(private firestore: AngularFirestore) { }

  	addFeedback(task,uid) {

  		
  		var docRef = this.firestore.collection('tasks').doc(this.uid)
	    var taskobj = {}
	    taskobj=task

	    docRef.update({tasks: firebase.firestore.FieldValue.arrayUnion(taskobj)}).then(() => {
	      
	    }).catch(e => {
	       docRef.set({tasks: taskobj})

	    })

  	}

	ngOnInit() {
	    
	    this.enrolledusers = []
	    this.uid = 'GQFvtptflcQ9ROrSXxPtamMa1Ek2'
	    //repalce by code to get the current authinticated user uuid
	    var docRef = this.firestore.collection('users').doc(this.uid)
	    docRef.get().subscribe(doc => {
	      if (doc.exists) {
	          
	          doc.data()["projects"].forEach((obj,index) => {
	            this.projects.push(obj)
	          });

	          this.projects.forEach((project,i) => {
	  	

	  	var docRef2 = this.firestore.collection('projects').doc(project)

	    docRef2.get().subscribe(doc => {
	      if (doc.exists) {
	          this.projectsdict[project] = new Array()
	          doc.data()["enrolled_users"].forEach((obj,index) => {
	            
	            this.projectsdict[project].push(obj)
	          });
	          
	          (Object.keys(this.projectsdict)).map((obj) => {
	          	this.projectsdict[obj].forEach((uid,index) => {
	          		var docRef3 = this.firestore.collection('users').doc(uid)
	          		docRef3.get().subscribe(doc => {
	      			if (doc.exists) {
	      				this.uidtousername[uid] = doc.data()["username"]
	      				var docRef4 = this.firestore.collection('tasks').doc(uid)

	      				docRef4.get().subscribe(doc => {
	      					this.tasksbyuid[uid] = doc.data()["tasks"]
	      				})
	      			}
	      		})


	          	})
	          })



	          
	      } else {
	          // doc.data() will be undefined in this case
	          console.log("No such document!");
	      }
	  }); 

	  })
	          

	          
	      } else {
	          // doc.data() will be undefined in this case
	          console.log("No such document!");
	      }
	  });


	  
	  console.log(this.projects)
	  console.log(this.projectsdict)
	  console.log(this.uidtousername)
	  console.log(this.tasksbyuid)

	  }

}
