import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyviewerComponent } from './companyviewer.component';

describe('CompanyviewerComponent', () => {
  let component: CompanyviewerComponent;
  let fixture: ComponentFixture<CompanyviewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyviewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyviewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
