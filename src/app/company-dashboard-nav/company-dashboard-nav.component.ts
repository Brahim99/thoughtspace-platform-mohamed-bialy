import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { CompanyDataService } from '../company-data.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { CurrentUserService } from '../current-user.service';

@Component({
  selector: 'app-company-dashboard-nav',
  templateUrl: './company-dashboard-nav.component.html',
  styleUrls: ['./company-dashboard-nav.component.css']
})
export class CompanyDashboardNavComponent implements OnInit {

  ready = false;
  pfpPath:string="";

  constructor(public currentUserService:CurrentUserService, private auth: AngularFireAuth, public router:Router, private routed:ActivatedRoute) { 
  }

  ngOnInit() {
  }

  toggleMenu(){
    const e = document.getElementById("profile-menu");
    if(e.style.display === "block")
      e.style.display = "none";
    else
      e.style.display = "block";
  }

  onClick(event) {
    const e = document.getElementById("profile-menu");
    if (!e.contains(event.target) && !document.getElementById("pf-picture").contains(event.target))
      e.style.display = "none";
  }

  hideMenu(){
    document.getElementById("profile-menu").style.display = "none";
  }

  account(){
    this.hideMenu();
    this.router.navigate(['dashboard'], {fragment: "account"});
  }

  logout(){
    localStorage.removeItem("uid");
    localStorage.removeItem("verified");
    localStorage.removeItem("intern");
    localStorage.removeItem("dashboardCache");
    localStorage.removeItem("dashboardCacheLastRef");
    this.currentUserService.logout();
    
    this.router.navigate(["/home"]);
  }

  getOrBlank(item:string){
    return (item !== null && item !== undefined) ? item.toLowerCase() : "";
  }

  includes(item:string, val:string){
    return item !== null && item !== undefined && item.toLowerCase().includes(val);
  }

  logoClick(){
    const seg = this.routed.snapshot.url;
    if(seg.length > 0 && (seg[0].path !== "dashboard" || (this.routed.snapshot.fragment !== null && this.routed.snapshot.fragment !== undefined)))
      this.router.navigate(['dashboard']);
    else
      this.router.navigate(['home']);
  }

  getCurrentView(){
    const seg = this.routed.snapshot.url;
    if(seg.length <= 0)
      return null;
    return seg[0].path;
  }

}
