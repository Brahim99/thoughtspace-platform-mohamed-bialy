import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyEngageSurveyComponent } from './company-engage-survey.component';

describe('CompanyEngageSurveyComponent', () => {
  let component: CompanyEngageSurveyComponent;
  let fixture: ComponentFixture<CompanyEngageSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyEngageSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyEngageSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
