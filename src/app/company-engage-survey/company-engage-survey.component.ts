import { Component, OnInit } from '@angular/core';
import {Label} from "ng2-charts";
import {CurrentUserService} from "../current-user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-company-engage-survey',
  templateUrl: './company-engage-survey.component.html',
  styleUrls: ['./company-engage-survey.component.css']
})
export class CompanyEngageSurveyComponent implements OnInit {

  constructor(public currentUserService: CurrentUserService, public router: Router) { }

  public chooseProjOpen:boolean = false;

  public ssLabels: Label[] = ["A", "B"];
  public ssColors = [
    { // grey
      backgroundColor: [
        'rgba(80, 80, 109,0.8)',
        'rgba(0,0,0,0)'
      ]
    }
  ];

  ngOnInit() {
  }

}
