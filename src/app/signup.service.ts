import { Injectable, EventEmitter, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  state:number = 0;
  showNav:boolean = false;
  farthest:number = 0;
  sectors:string[] = [];
  softSkills:string[] = [];
  hardSkills:string[] = [];

  @Output() doJump: EventEmitter<any> = new EventEmitter();
  constructor() { }

  jump(state){
    if(state <= this.farthest){

      this.doJump.emit(null);

      var list = document.getElementsByClassName("s-" + this.state);
      let valid = true;

      for(let i = 0; i < list.length; i++){
        const el = <HTMLInputElement>list[i];
        if(el.classList.contains("invalid")){
          valid = false;
          break;
        }
      }
      
      if(valid)
        this.state = state;
    }
  }
}