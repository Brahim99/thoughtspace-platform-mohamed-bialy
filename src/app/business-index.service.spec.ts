import { TestBed } from '@angular/core/testing';

import { BusinessIndexService } from './business-index.service';

describe('BusinessIndexService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BusinessIndexService = TestBed.get(BusinessIndexService);
    expect(service).toBeTruthy();
  });
});
