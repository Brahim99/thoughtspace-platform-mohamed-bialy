import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpWhyTsComponent } from './lp-why-ts.component';

describe('LpWhyTsComponent', () => {
  let component: LpWhyTsComponent;
  let fixture: ComponentFixture<LpWhyTsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpWhyTsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpWhyTsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
