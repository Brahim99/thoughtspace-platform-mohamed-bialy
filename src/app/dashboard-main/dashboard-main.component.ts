import { Component, OnInit, ViewChild } from '@angular/core';
import { DashboardViewComponent } from '../dashboard-view/dashboard-view.component';

@Component({
  selector: 'app-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.css']
})
export class DashboardMainComponent implements OnInit {

  @ViewChild('dView') dView: DashboardViewComponent;
  constructor() { }

  ngOnInit() {
  }

}
