import { Component, OnInit } from '@angular/core';
import { TaskTimerService } from '../task-timer.service';

@Component({
  selector: 'app-task-view',
  templateUrl: './task-view.component.html',
  styleUrls: ['./task-view.component.css']
})
export class TaskViewComponent implements OnInit {

  state = 0;
  number = 50;
  arr:number[];
  time = 0;

  constructor(public timer: TaskTimerService) { }

  ngOnInit() {
    this.arr = Array.from({length:10}, (value, key) => key);
  }

  getArray(n: number){
    return Array.from({length:n}, (value, key) => key)
  }

  begin(){
    this.state++;
    this.timer.addTask("BITCOINS");
  }

  click(event){
    const ele = <HTMLElement> event.target;
    ele.parentNode.removeChild(ele);
    this.number--;

    if(this.number == 0)
      this.win();
  }

  win(){
    this.time = this.timer.completeTask("BITCOINS");
    this.state++;
  }

  getTime(){

    if(this.state < 2){
      this.time = this.timer.getTime("BITCOINS");
      if(this.time < 0) 
        this.time = 0;
    }

    let sec = this.time % 60;
    let min = Math.floor(this.time / 60);
    let hour = Math.floor(min / 60);
    min = min % 60;

    let s = (Math.floor(sec * 100) / 100) + "";
    if(sec < 10) s = "0" + s;
    if(s.length == 4) s += "0";
    if(s.length < 4) s += ".00";

    return this.double(hour) + ":" + this.double(min) + ":" + s;

  }

  double(num){
    const s = num+"";
    if(s.length < 2) return "0" + s;
  }

}
