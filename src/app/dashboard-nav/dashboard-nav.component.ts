import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { DataService } from '../data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { InternsCacheService, Intern } from '../interns-cache.service';
import { CurrentUserService } from '../current-user.service';

@Component({
  selector: 'app-dashboard-nav',
  templateUrl: './dashboard-nav.component.html',
  styleUrls: ['./dashboard-nav.component.css'],
  host: {
    '(document:click)': 'onClick($event)',
  }
})
export class DashboardNAVComponent implements OnInit {

  // @Input() profilePicUpload: ProfilePicUploadComponent;
  @Input() fixed:boolean;
  @Input() hasSearch:boolean = false;
  prompts:Intern[];

  constructor(public currentUserService:CurrentUserService, public router:Router, private internCache: InternsCacheService, private routed:ActivatedRoute) { 
  }

  ngOnInit() {
  }

  ngAfterViewInit(){
    if(this.fixed)
      document.getElementById("nav").style.position = "fixed";
  }

  forceExiting = false;
  editDesc(){
    this.forceExiting = false;
    document.getElementById("descShow").style.display = "none";
    document.getElementById("descEdit").style.display = "block";
    document.getElementById("descEdit").focus();
  }

  onFinishEdit(){
    if(!this.forceExiting){
      const val = (<HTMLInputElement> document.getElementById("descEdit")).value;
      // if(this.data.skillsInfo['description'] !== val)
      //   this.data.updateDesc(val);
    }
    document.getElementById("descEdit").style.display = "none";
    document.getElementById("descShow").style.display = "block";
    this.forceExiting = false;
  }

  adjustTextArea(o){
    o.style.height = "1px";
    o.style.height = (14 + o.scrollHeight)+"px";
  }

  keydown(event){
    if(event.keyCode === 27){
      this.forceExiting = true;
      document.getElementById("descEdit").blur();
    }
  }

  searchBar(event){
    if(event.keyCode === 13 && event.target.value.length > 0)
      this.router.navigate(["search/" + event.target.value]);
  }

  toggleMenu(){
    const e = document.getElementById("profile-menu");
    if(e.style.display === "block")
      e.style.display = "none";
    else
      e.style.display = "block";
  }

  onClick(event) {
    const e = document.getElementById("profile-menu");
    if (!e.contains(event.target) && !document.getElementById("pf-picture").contains(event.target))
      e.style.display = "none";
  }

  hideMenu(){
    document.getElementById("profile-menu").style.display = "none";
  }

  account(){
    this.hideMenu();
    this.router.navigate(['dashboard'], {fragment: "account"});
  }

  logout(){
    localStorage.removeItem("uid");
    localStorage.removeItem("verified");
    localStorage.removeItem("intern");
    localStorage.removeItem("dashboardCache");
    localStorage.removeItem("dashboardCacheLastRef");
    this.currentUserService.logout();
    
    this.router.navigate(["/home"]);
  }

  searchBarDown(event){
    if(event.keyCode === 13 && event.target.value.length > 0)
      this.router.navigate(["search/" + event.target.value]);
  }

  async onFocus(){
    this.prompts = await this.internCache.getInterns();
  }

  blur(){
    setTimeout(() => this.prompts = null, 500);
  }

  getOrBlank(item:string){
    return (item !== null && item !== undefined) ? item.toLowerCase() : "";
  }

  includes(item:string, val:string){
    return item !== null && item !== undefined && item.toLowerCase().includes(val);
  }

  async searchBarUp(event){
    if(event.keyCode === 27)
      event.target.blur();
    else if(event.keyCode !== 13){
      const interns:Intern[] = await this.internCache.getInterns();
      if(event.target.value.length > 0){
        const val = event.target.value;
        this.prompts = interns.filter(intern => 
          (this.getOrBlank(intern.mainInfo['firstname']) + " " + this.getOrBlank(intern.mainInfo['lastname'])).includes(val)
          || this.includes(intern.mainInfo['university'], val)
        );
      }
      else
        this.prompts = interns;
    }
  }

  switch(username:string){
    this.prompts = null;
    this.router.navigate(['profiles/' + username]);
  }

  logoClick(){
    const seg = this.routed.snapshot.url;
    if(seg.length > 0 && (seg[0].path !== "dashboard" || (this.routed.snapshot.fragment !== null && this.routed.snapshot.fragment !== undefined)))
      this.router.navigate(['dashboard']);
    else
      this.router.navigate(['home']);
  }

  getCurrentView(){
    const seg = this.routed.snapshot.url;
    if(seg.length <= 0)
      return null;
    return seg[0].path;
    // if(seg.length <= 0 || seg[0].path !== "dashboard")
    //   return null;
    // if(this.routed.snapshot.fragment === null || this.routed.snapshot.fragment === undefined)
    //   return "";
    // return this.routed.snapshot.fragment;
  }
}
