import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardNAVComponent } from './dashboard-nav.component';

describe('DashboardNAVComponent', () => {
  let component: DashboardNAVComponent;
  let fixture: ComponentFixture<DashboardNAVComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardNAVComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardNAVComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
