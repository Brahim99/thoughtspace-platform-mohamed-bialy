import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpTaketourComponent } from './lp-taketour.component';

describe('LpTaketourComponent', () => {
  let component: LpTaketourComponent;
  let fixture: ComponentFixture<LpTaketourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpTaketourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpTaketourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
