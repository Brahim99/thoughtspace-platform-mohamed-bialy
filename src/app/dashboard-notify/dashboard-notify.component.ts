import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-dashboard-notify',
  templateUrl: './dashboard-notify.component.html',
  styleUrls: ['./dashboard-notify.component.css']
})
export class DashboardNotifyComponent implements OnInit {

  constructor(public data: DataService) { }

  ngOnInit() {
  }

  hide(){
    document.getElementById('container').style.display = 'none';
  }

}
