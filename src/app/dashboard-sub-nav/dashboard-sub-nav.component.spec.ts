import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSubNavComponent } from './dashboard-sub-nav.component';

describe('DashboardSubNavComponent', () => {
  let component: DashboardSubNavComponent;
  let fixture: ComponentFixture<DashboardSubNavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSubNavComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSubNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
