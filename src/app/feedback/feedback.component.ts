import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentUserService, Project } from '../current-user.service';
import { MatSliderChange } from '@angular/material';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  viewing: Project;
  internUID: string;
  intern: {[field: string]: any};


  constructor(public routed: ActivatedRoute, public router: Router, public firestore: AngularFirestore, public currentUserService: CurrentUserService) { }

  ngOnInit() {

    window.scrollTo(0, 0);
    
    this.routed.url.subscribe(url => {
      if(url != null && url != undefined && url.length > 3 && url[1].path == "evaluate"){
        const name = url[2].path;
        const intern = url[3].path;
        
        this.currentUserService.runWhenReady(() => {
          const project = this.currentUserService.currentUser.company.projects[name];
          if(project == undefined)
            this.router.navigate(["dashboard"]);
          else if(project.matches == undefined || project.matches == null || !(intern in project.matches))
            this.router.navigate(["dashboard/project/" + project.name]);
          else
            this.load(project, intern);
        });
      }
    });

  }

  onSlide(event: MatSliderChange){
    const element = <HTMLElement> event.source._elementRef.nativeElement;
    const display = <HTMLElement> (element.parentElement.childNodes[1].childNodes[1]);
    display.innerHTML = event.value + "";
  }

  load(project: Project, intern: string){
    this.viewing = project;
    this.intern = project.matches[intern];
    this.internUID = intern;

    this.firestore.doc("business_page/" + this.viewing.domain + "/projects/" + this.viewing.name + "/evaluation/" + this.internUID).get().toPromise().then(doc => {
      if(doc.exists){
        for(let skill in doc.data()["hardSkills"]){
          const value = doc.data()["hardSkills"][skill];
          document.getElementById(skill).innerHTML = value + "";
          this.setSliderVal(skill+"_slider", value, 10);
        }

        document.getElementById("softSkills").innerHTML = doc.data()["softSkills"] + "";
        this.setSliderVal("softSkills_slider", doc.data()["softSkills"], 100);

        (<HTMLInputElement> document.getElementById("comments")).value = doc.data()["comments"] + "";
      }
    });
  }

  setSliderVal(id, value:number, max:number){
    const el = document.getElementById(id);
    const wr = el.children[0]; // mat slider wrapper
    const left = value/max * 100; // distance from left
    const right = 100 - left; // distance from right
    (<HTMLElement> wr.children[0].children[0]).style.transform = "translateX(0px) scale3d(" + right/100 + ", 1, 1)";
    (<HTMLElement> wr.children[0].children[1]).style.transform = "translateX(0px) scale3d(" + left/100 + ", 1, 1)";
    (<HTMLElement> wr.children[2]).style.transform = "translateX(-" + right + "%)";
  }

  cancel(){
    this.router.navigate(["dashboard/project/" + this.viewing.name]);
  }

  submit(){
    // const skills:{[field: string]: number} = {};
    // for(let skill of this.intern['sharedskills'])
    //   skills[skill] = +document.getElementById(skill).innerHTML;

    // const soft = +document.getElementById("softSkills").innerHTML;
    // const comments = (<HTMLInputElement> document.getElementById("comments")).value;

    // this.firestore.doc("business_page/" + this.viewing.domain + "/projects/" + this.viewing.name + "/evaluation/" + this.internUID).set({
    //   "softSkills": soft,
    //   "hardSkills": skills,
    //   "comments": comments
    // });

    this.router.navigate(["dashboard/project/" + this.viewing.name]);
  }

}
