import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VerifyService {

  constructor(private http:HttpClient) { }

  sendVerification(uid){
    return this.http.post("https://cors-anywhere.herokuapp.com/https://us-central1-devproduct.cloudfunctions.net/authfunctions-GenerateVerificationEmail", {
      "uid": uid
    }).toPromise();
  }
}
