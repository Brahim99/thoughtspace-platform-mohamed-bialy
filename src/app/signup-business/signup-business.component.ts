import { Component, OnInit } from '@angular/core';
import { SignupBusinessService } from '../signup-business.service';

@Component({
  selector: 'app-signup-business',
  templateUrl: './signup-business.component.html',
  styleUrls: ['./signup-business.component.css']
})
export class SignupBusinessComponent implements OnInit {

  constructor(public signupService:SignupBusinessService) { }

  ngOnInit() {
  }

  jump(state){
    this.signupService.jump(state);
  }

}
