import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupBusinessComponent } from './signup-business.component';

describe('SignupBusinessComponent', () => {
  let component: SignupBusinessComponent;
  let fixture: ComponentFixture<SignupBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
