import { Component, OnInit, Input } from '@angular/core';

import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';

export interface SkillGroup {
  category: string;
  skills: string[];
  hidden: boolean;
}

@Component({
  selector: 'app-soft-skills',
  templateUrl: './soft-skills.component.html',
  styleUrls: ['./soft-skills.component.css']
})
export class SoftSkillsComponent implements OnInit {

  @Input() company:boolean = false;
  ready:boolean = false;
  
  // skills:string[] = ["Communication", "Experience", "Leadership", "Problem Solving", "Professionalism", "Self-Management", "Teamwork"];
  groups:SkillGroup[] = [];
  default:SkillGroup[] = [
    {
      category: "Communication",
      skills: ["Accurate communication", "Appropriate communication through social media", "Asking good questions", "Effective oral communication", "Effective written communication", "Listening effectively", "Professional and pleasant communication"],
      hidden: true
    },
    {
      category: "Decision Making",
      skills: ["Creative and innovative solutions", "Engaging in life-long learning", "Identifying and analyzing problems", "Realizing the effect of decisions", "Taking effective and appropiate actions", "Thinking abstractly about problems", "Transferring knowledge from one situation to another"],
      hidden: true
    },
    {
      category: "Self-Management",
      skills: ["Adopting and applying appropriate technology", "Dedication to continued improvement", "Efficient and effective work management", "Self-starting and self-learning", "Sense of urgency to complete tasks", "Well-developed sense of integrity and ethics", "Working well under pressure"],
      hidden: true
    },
    {
      category: "Teamwork",
      skills: ["Aware and sensitive to industry", "Maintains accountability with the team", "Positive and encouraging attitude", "Productive as a team member", "Punctual and meets deadlines", "Sharing ideas to multiple audiences", "Works with multiple approaches"],
      hidden: true
    },
    {
      category: "Professionalism",
      skills: ["Accepting and applying critical feedback", "Dealing effectively with ambiguity", "Effective relationship with customers, businesses, and the public", "Maintaining appropiate demeanor", "Selecting appropiate mentors", "Trustworthy with sensitive information", "Understanding role and realistic career expectations"],
      hidden: true
    },
    {
      category: "Experience",
      skills: ["Community engagement experiences", "Cross-disciplinary experiences", "International experiences", "Leadership experiences", "Project management experiences", "Related work or internship experience", "Teamwork experience"],
      hidden: true
    },
    {
      category: "Leadership",
      skills: ["Building professional relationships", "Motivating and leading others", "Recognizing and dealing effectively with conflict", "Recognizing change is needed and implement necessary actions", "Recognizing when to lead and when to follow", "Respecting and accepting contributions from others", "Seeing the big picture and thinking strategically"],
      hidden: true
    }
  ];

  ngOnInit() {
    if(!this.ready)
      this.reset();
    this.ready = true;
  }

  reset(){
    this.groups = [];
    for(const group of this.default){
      const clonedSkills = group.skills.map(x => x+"");
      const sGroup : SkillGroup = {
        category: group.category,
        skills: clonedSkills,
        hidden: true
      };
      this.groups.push(sGroup);
    }
  }

  getRankedList() : {[field: string]: any}[] {
    const list:{[field: string]: any}[] = [];
      
    let i = 0;
   
    for(const group of this.groups){
      
      const skills = {};

      skills["name"] = group.category;
      skills["rank"] = i;
      skills["skills"] = group.skills;
      
      list.push(skills);
      
      i++;
    }

    return list;
  }

  setRankedList(list: {[field: string]: any}[]) {
    this.ready = true;

    for(const group of list){
      const sGroup : SkillGroup = {
        category: group["name"],
        skills: group["skills"],
        hidden: true
      };

      this.groups[group["rank"]] = sGroup;
    }

  }

  dropSkill(event: CdkDragDrop<string[]>) {
    for(const group of this.groups){
      if(group.category === event.container.id){
        moveItemInArray(group.skills, event.previousIndex, event.currentIndex);
        return;
      }
    }
  }

  dropGroup(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.groups, event.previousIndex, event.currentIndex);
  }

  getConnectedList(): any[] {
    return this.groups.map(x => `${x.category}`);
  }

  clickGroup(event: MouseEvent, category: string){
    if(event.srcElement.classList.contains("skill")) return;
    
    for(const group of this.groups){
      if(group.category === category){
        group.hidden = !group.hidden;
        document.getElementById(category + "_b").innerHTML = group.hidden ? "+" : "-";
        return;
      }
    }
  }
}
