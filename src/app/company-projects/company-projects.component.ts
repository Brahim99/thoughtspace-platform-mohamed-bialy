import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../current-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-projects',
  templateUrl: './company-projects.component.html',
  styleUrls: ['./company-projects.component.css']
})
export class CompanyProjectsComponent implements OnInit {

  view:string = "all";

  constructor(public currentUserService: CurrentUserService, public router: Router) { }

  ngOnInit() {
  }

}
