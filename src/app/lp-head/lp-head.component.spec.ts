import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpHeadComponent } from './lp-head.component';

describe('LpHeadComponent', () => {
  let component: LpHeadComponent;
  let fixture: ComponentFixture<LpHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
