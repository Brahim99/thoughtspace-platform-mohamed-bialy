import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lp-head',
  templateUrl: './lp-head.component.html',
  styleUrls: ['./lp-head.component.css']
})
export class LpHeadComponent implements OnInit {

  constructor(public router: Router) { }

  ngOnInit() {
  }

}
