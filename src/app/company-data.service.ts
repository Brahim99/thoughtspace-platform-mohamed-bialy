import { Injectable, EventEmitter } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CompanyDataService {

  public mainInfo:{[field: string]: any} = {};
  public profileInfo:{[field: string]: any} = {};
  public notifier:EventEmitter<void> = new EventEmitter();
  public alreadyReady = false;
  public domain:string;
  public pfpPath:string;
  
  constructor(public firestore: AngularFirestore, public router:Router) {
  }

  async reload(domain:string){
    
    this.domain = domain;

    const doc = await this.firestore.doc("business_page/" + this.domain).get().toPromise();
    this.mainInfo = doc.data();

    const profile = await this.firestore.doc("business_page/" + this.domain + "/public_profile/main").get().toPromise();
    this.profileInfo = profile.data();
    const personalProfile = await this.firestore.doc("business_users/" + localStorage.getItem("uid") + "/profile/main").get().toPromise();
    this.pfpPath = personalProfile.data()['profilePic'];

    this.notifier.emit();
    this.alreadyReady = true;

  }

}
