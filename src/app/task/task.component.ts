import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
//import {TaskviewerComponent} from '../task/task.component'
import { TaskviewerComponent } from '../taskviewer/taskviewer.component';
var firebase = require("firebase/app")
firebase.initializeApp({
    apiKey: "AIzaSyDpJmDgiEzYVxzgHxsCXfDk5mAzU1xb1tU",
    authDomain: "devproduct.firebaseapp.com",
    databaseURL: "https://devproduct.firebaseio.com",
    projectId: "devproduct",
    storageBucket: "devproduct.appspot.com",
    messagingSenderId: "717831524965",
    appId: "1:717831524965:web:35b6a9fdd34bb6a9"

})
@Component({
  providers:[TaskviewerComponent],
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {

  title: string = ''
  description: string = ''
  dueDate: Date = new Date()
  tasktype: string = ''
  uid:string = ''
  projects:string[]  = new Array()
  project:string = ''


  constructor(private firestore: AngularFirestore,private taskviewer: TaskviewerComponent) { 
    


  }

  add() {
    var docRef = this.firestore.collection('tasks').doc(this.uid)
    var taskobj = {}
    taskobj={project_name:this.project,
              due_date:this.dueDate,
              title:this.title,
              description : this.description,
              feedback: '',
              notification : 0
    
    }

    docRef.update({tasks: firebase.firestore.FieldValue.arrayUnion(taskobj)}).then(() => {
      this.taskviewer.tasks = []
      this.taskviewer.ngOnInit()
    }).catch(e => {
       docRef.set({tasks: taskobj})

    })
  }

  ngOnInit() {
    console.log("init")

    //this.uid = '2jmQhoSBMPPNJXjfmslRuSSCiGH3' 
    this.uid = firebase.auth().currentUser.uid
    //console.log(this.uid)
    //repalce by code to get the current authinticated user uuid
    var docRef = this.firestore.collection('users').doc(this.uid)
    docRef.get().subscribe(doc => {
      if (doc.exists) {
          
          doc.data()["projects"].forEach((obj,index) => {
            this.projects.push(obj)
          });
          console.log(this.projects)

          
      } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
      }
  });

      

  }

}
