import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ChartsModule} from "ng2-charts";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { HeadComponent } from './head/head.component';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoginComponent } from './login/login.component';
import { VerifyComponent } from './verify/verify.component';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatSlider, MatSliderModule, MatRadioModule } from '@angular/material';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { InterestsComponent } from './interests/interests.component';
import { HardSkillsComponent } from './hard-skills/hard-skills.component';
import { SoftSkillsComponent } from './soft-skills/soft-skills.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DashboardNAVComponent } from './dashboard-nav/dashboard-nav.component';
import { DashboardMainComponent } from './dashboard-main/dashboard-main.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { LpNavComponent } from './lp-nav/lp-nav.component';
import { HttpClientModule } from '@angular/common/http';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { DropZoneDirective } from './drop-zone.directive';
import { ProfilePicUploadComponent } from './profile-pic-upload/profile-pic-upload.component';
import { LpHeadComponent } from './lp-head/lp-head.component';
import { LpServicesComponent } from './lp-services/lp-services.component';
import { LpWhyTsComponent } from './lp-why-ts/lp-why-ts.component';
import { SignupBusinessComponent } from './signup-business/signup-business.component';
import { BasicInfoBusinessComponent } from './basic-info-business/basic-info-business.component';
import { CompanyPageComponent } from './company-page/company-page.component';
import { LpPlatformFeaturesComponent } from './lp-platform-features/lp-platform-features.component';
import { SearchComponent } from './search/search.component';
import { DashboardMenuComponent } from './dashboard-menu/dashboard-menu.component';
import { DashboardViewComponent } from './dashboard-view/dashboard-view.component';
import { SearchPromptComponent } from './search-prompt/search-prompt.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardAccountComponent } from './dashboard-account/dashboard-account.component';
import { DashboardNotifyComponent } from './dashboard-notify/dashboard-notify.component';
import { DashboardNotifySettingsComponent } from './dashboard-notify-settings/dashboard-notify-settings.component';
import { BasicInfoUniversityComponent } from './basic-info-university/basic-info-university.component';
import { BasicInfoCareerComponent } from './basic-info-career/basic-info-career.component';
import { DashboardWorkspaceComponent } from './dashboard-workspace/dashboard-workspace.component';
import { DashboardTasksComponent } from './dashboard-tasks/dashboard-tasks.component';
// import { DashboardTimetrackerComponent } from './dashboard-timetracker/dashboard-timetracker.component';
import { WorkspaceActionsComponent } from './workspace-actions/workspace-actions.component';
import { WorkspaceWorkhistoryComponent } from './workspace-workhistory/workspace-workhistory.component';
import { WorkspaceStatsComponent } from './workspace-stats/workspace-stats.component';
import { DashboardInternshipsComponent } from './dashboard-internships/dashboard-internships.component';
import { DashboardEngageComponent } from './dashboard-engage/dashboard-engage.component';
import { DashboardRewardsComponent } from './dashboard-rewards/dashboard-rewards.component';
import { TaskViewComponent } from './task-view/task-view.component';
import { CompanyDashboardComponent } from './company-dashboard/company-dashboard.component';
import { CompanyDashboardNavComponent } from './company-dashboard-nav/company-dashboard-nav.component';
import { LpTaketourComponent } from './lp-taketour/lp-taketour.component';
import { LpFooterComponent } from './lp-footer/lp-footer.component';
import { CompanyCreateprojectComponent } from './company-createproject/company-createproject.component';
import { CompanyViewprojectComponent } from './company-viewproject/company-viewproject.component';
import { DashboardWelcomeComponent } from './dashboard-welcome/dashboard-welcome.component';
import { DashboardSubNavComponent } from './dashboard-sub-nav/dashboard-sub-nav.component';
import { DashboardCompleteprofileComponent } from './dashboard-completeprofile/dashboard-completeprofile.component';
import { FormStudentComponent } from './form-student/form-student.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { ChatbotComponent } from './chatbot/chatbot.component';
import { SurveyComponent } from './survey/survey.component';
import { RlpDemoComponent } from './rlp-demo/rlp-demo.component';
import { TaskComponent } from './task/task.component';
import {TaskviewerComponent} from './taskviewer/taskviewer.component';
import { CompanyDashboardOverviewComponent } from './company-dashboard-overview/company-dashboard-overview.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { CompanyProjectsComponent } from './company-projects/company-projects.component';
import { CompanyAnalyticsComponent } from './company-analytics/company-analytics.component';
import { CompanyFeedbackComponent } from './company-feedback/company-feedback.component';
import { StudentEvaluateComponent } from './student-evaluate/student-evaluate.component';
import { CompanyEngageChallengeComponent } from './company-engage-challenge/company-engage-challenge.component';
import { CompanyEngageSurveyComponent } from './company-engage-survey/company-engage-survey.component';
import { CompanyEngageWebinarComponent } from './company-engage-webinar/company-engage-webinar.component';
import { CompanyEngageCourseComponent } from './company-engage-course/company-engage-course.component';
import { DashboardPracticeComponent } from './dashboard-practice/dashboard-practice.component';
import { DashboardEvaluateComponent } from './dashboard-evaluate/dashboard-evaluate.component';
import { CompanyCalendarComponent } from './company-calendar/company-calendar.component';
import { CompanyviewerComponent } from './companyviewer/companyviewer.component';
import { PlaygroundComponent } from './playground/playground.component';


@NgModule({
  declarations: [
    AppComponent,
    TaskviewerComponent,
    NavComponent,
    HeadComponent,
    BasicInfoComponent,
    DashboardComponent,
    SignupComponent,
    LoginComponent,
    VerifyComponent,
    PasswordResetComponent,
    InterestsComponent,
    HardSkillsComponent,
    SoftSkillsComponent,
    DashboardNAVComponent,
    DashboardMainComponent,
    LandingPageComponent,
    LpNavComponent,
    EmailVerificationComponent,
    DropZoneDirective,
    ProfilePicUploadComponent,
    LpHeadComponent,
    LpServicesComponent,
    LpWhyTsComponent,
    SignupBusinessComponent,
    BasicInfoBusinessComponent,
    CompanyPageComponent,
    LpPlatformFeaturesComponent,
    SearchComponent,
    DashboardMenuComponent,
    DashboardViewComponent,
    SearchPromptComponent,
    ProfileComponent,
    DashboardAccountComponent,
    DashboardNotifyComponent,
    DashboardNotifySettingsComponent,
    BasicInfoUniversityComponent,
    BasicInfoCareerComponent,
    DashboardWorkspaceComponent,
    DashboardTasksComponent,
    // DashboardTimetrackerComponent,
    WorkspaceActionsComponent,
    WorkspaceWorkhistoryComponent,
    WorkspaceStatsComponent,
    DashboardInternshipsComponent,
    DashboardEngageComponent,
    DashboardRewardsComponent,
    TaskViewComponent,
    CompanyDashboardComponent,
    CompanyDashboardNavComponent,
    LpTaketourComponent,
    LpFooterComponent,
    CompanyCreateprojectComponent,
    CompanyViewprojectComponent,
    DashboardWelcomeComponent,
    DashboardSubNavComponent,
    DashboardCompleteprofileComponent,
    FormStudentComponent,
    FileUploadComponent,
    ChatbotComponent,
    SurveyComponent,
    RlpDemoComponent,
    TaskComponent,
    CompanyDashboardOverviewComponent,
    FeedbackComponent,
    CompanyProjectsComponent,
    CompanyAnalyticsComponent,
    CompanyFeedbackComponent,
    StudentEvaluateComponent,
    CompanyEngageChallengeComponent,
    CompanyEngageSurveyComponent,
    CompanyEngageWebinarComponent,
    CompanyEngageCourseComponent,
    DashboardPracticeComponent,
    DashboardEvaluateComponent,


    CompanyCalendarComponent,
    CompanyviewerComponent,
    PlaygroundComponent
  ],
  imports: [
    ChartsModule,
    BrowserModule,
    DragDropModule,
    AppRoutingModule,
    MatFormFieldModule,
    MatSliderModule,
    MatRadioModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatAutocompleteModule,
    HttpClientModule,
    AngularFireStorageModule,
    AngularFireModule.initializeApp(environment.firebase),
  ],
  providers: [AngularFireAuth, AngularFirestore],
  bootstrap: [AppComponent]
})
export class AppModule { }
