import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ProfilePicUploadComponent } from '../profile-pic-upload/profile-pic-upload.component';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-account',
  templateUrl: './dashboard-account.component.html',
  styleUrls: ['./dashboard-account.component.css']
})
export class DashboardAccountComponent implements OnInit {

  public ppUpload: ProfilePicUploadComponent;

  constructor(public data: DataService, public firestore: AngularFirestore, public router: Router) { }

  ngOnInit() {
  }

  formatPhone(tel:string){
    if(tel === null || tel === undefined)
      return "";
    
    let raw = tel.replace(/[^0-9]/g, '');
    
    let formatted = "";

    if(raw.length > 0){
      formatted = "(";
      if(raw.length > 3){
        formatted += raw.slice(0, 3) + ") ";

        if(raw.length > 6)
          formatted += raw.slice(3, 6) + "-" + raw.slice(6, 10);
        else
          formatted += raw.slice(3);
      }
      else
        formatted += raw;
    }

    return formatted;
  }

  format(target, tel:string){
    let raw = tel.replace(/[^0-9]/g, '');
    let good:boolean = raw.length >= 10;
    
    let formatted = "";

    if(raw.length > 0){
      formatted = "(";
      if(raw.length > 3){
        formatted += raw.slice(0, 3) + ") ";

        if(raw.length > 6)
          formatted += raw.slice(3, 6) + "-" + raw.slice(6, 10);
        else
          formatted += raw.slice(3);
      }
      else
        formatted += raw;
    }

    target.value = formatted;

    return good;
  }

  editPFP(){
    this.ppUpload.open = true;
  }

  getElements(...args: string[]){
    const list:HTMLInputElement[] = [];
    for(const arg of args)
      list.push(<HTMLInputElement>document.getElementById(arg));
    return list;
  }

  onChange(target){

    let valid = false;

    if(target.type === "tel")
      valid = this.format(target, target.value);
    else if(target.id === "zipcode"){
      target.value = target.value.replace(/[^0-9]/g, '').slice(0, 5);
      valid = target.value.length === 5;
    }
    else if(target.id === "username"){
      target.value = target.value.replace(/[^a-z\-A-Z\_0-9]/g, '').slice(0, 32);
      valid = target.value.length > 0;

      if(valid && target.value !== this.data.mainInfo['username']){
        this.firestore.collection("users").ref.where("username", "==", target.value).get().then(collection => {
          if(collection.docs.length > 0){
            target.classList.remove('changed');
            target.classList.add('invalid');
            document.getElementById("errUser").style.display = "block";
          }
          else {
            target.classList.remove('invalid');
            target.classList.add('changed');
            document.getElementById("errUser").style.display = "none";
          }
        });
      }
    }
    else {
      valid = target.value.length > 0;
      target.value = target.value.slice(0, 64);
    }

    if(valid){
      target.classList.remove('invalid');
      let raw = target.value;
      if(target.type === "tel")
        raw = target.value.replace(/[^0-9]/g, '');
      if(raw !== this.data.mainInfo[target.id])
        target.classList.add('changed');
      else
        target.classList.remove('changed');
    }
    else {
      target.classList.remove('changed');
      target.classList.add('invalid');
    }

  }

  async attemptSubmit(){
    const list:HTMLInputElement[] = this.getElements("username", "phone", "street", "city", "state", "country", "zipcode");

    const diff:{[field:string]: string} = {};
    let total:number = 0;

    const msg = document.getElementById("msg");
    msg.style.display = "block";
    msg.innerHTML = "Saving data...";
    msg.style.color = "goldenrod";

    for(let el of list){
      let raw = el.value;
      if(el.type === "tel")
        raw = el.value.replace(/[^0-9]/g, '');
      
      if(raw !== this.data.mainInfo[el.id]){

        if(el.classList.contains("invalid")){
          el.scrollIntoView();
          msg.innerHTML = "Please correct the " + el.id + " field!";
          msg.style.color = "red";
          return;
        }

        if(el.id === "username"){
            const collection = await this.firestore.collection("users").ref.where("username", "==", raw).get();
            if(collection.docs.length > 0){
              el.classList.remove('changed');
              el.classList.add('invalid');
              document.getElementById("errUser").style.display = "block";
              el.scrollIntoView();
              msg.innerHTML = "Please correct the " + el.id + " field!";
              msg.style.color = "red";
              return;
            }
            else
              document.getElementById("errUser").style.display = "none";
        }

        diff[el.id] = raw;
        total++;
      }
      
    }

    if(total > 0){
      for(let key in diff){
        this.data.mainInfo[key] = diff[key];
        this.data.cached.mainInfo[key] = diff[key];
        this.data.updateCache();
      }

      await this.firestore.doc("intern_users/" + localStorage.getItem("uid")).update(diff);
      await this.firestore.doc("users/" + localStorage.getItem("uid")).update(diff);

      msg.innerHTML = "Profile updated!";
      msg.style.color = "green";

      for(let key in diff)
        document.getElementById(key).classList.remove("changed");
    }
    else {
      msg.innerHTML = "No changes to save!";
      msg.style.color = "red";
    }
  }

  changePass(){
    localStorage.setItem("prEmail", this.data.mainInfo['email']);
    this.router.navigate(["passwordreset"]);
  }
}
