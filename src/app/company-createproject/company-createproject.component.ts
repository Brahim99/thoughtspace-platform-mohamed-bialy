import { Component, OnInit, ViewChild, Input, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { HardSkillsComponent } from '../hard-skills/hard-skills.component';
import { SoftSkillsComponent } from '../soft-skills/soft-skills.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Project, CurrentUserService } from '../current-user.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-company-createproject',
  templateUrl: './company-createproject.component.html',
  styleUrls: ['./company-createproject.component.css']
})
export class CompanyCreateprojectComponent implements OnInit {

  @ViewChildren("hardSkills") hardSkills: QueryList<HardSkillsComponent>;
  @ViewChild("softSkills") softSkills: SoftSkillsComponent;
  public editing: Project = null;
  public projectname:string = "";
  public step:number = 0;
  public interns:number[] = [1];
  public max = 0;
  public additional:string[] = [];
  public summarySkills:string[] = [];

  constructor(public firestore: AngularFirestore, public router: Router, public routed: ActivatedRoute, public currentUserService: CurrentUserService, private http:HttpClient) { }

  ngOnInit() {
    const url = this.routed.snapshot.url;
    if(url != null && url != undefined && url.length > 2){
      const name = url[2].path;
      const project = this.currentUserService.currentUser.company.projects[name];
      if(project == undefined)
        this.router.navigate(["dashboard"]);
      else
        this.load(this.currentUserService.currentUser.company.projects[name]);
    }
  }

  get(name) : HTMLInputElement {
    return <HTMLInputElement> document.getElementById(name);
  }

  async load(project:Project){
    this.editing = project;
    this.projectname = project.name;

    this.get("name").readOnly = true;
    this.get("name").value = project.name;
    this.get("description").value = project.description;
    this.get("hours").value = project.hours+"";
    // this.get("interns").value = project.interns+"";
    this.get("weeks").value = project.weeks+"";
    this.get("location").value = project.location;
    if(project.role !== null && project.role !== undefined && project.role !== "")
      this.get("primaryRole").value = project.role;
    this.clearSelect();
    // this.select(document.getElementById(project.types));
    // for(let type of project.types)
    //   this.select(document.getElementById(type));

    // this.hardSkills.setSelectedList(project.hardSkills);
    this.softSkills.setRankedList(project.softSkills);
    this.step = 0;
    this.max = 0;
    this.setProgress();
  }

  clear(element: HTMLInputElement){
    element.value = "";
    element.classList.remove("invalid");
  }

  reset(){
    this.editing = null;
    this.projectname = "";
    this.get("name").readOnly = false;
    this.clear(this.get("name"));
    this.clear(this.get("description"));
    this.clear(this.get("hours"));
    // this.clear(this.get("interns"));
    this.clear(this.get("weeks"));
    this.clear(this.get("location"));
    this.clearSelect();
    // this.select(document.getElementById("SD"));
    // this.hardSkills.setSelectedList([]);
    this.softSkills.reset();
    this.step = 0;
    this.max = 0;
    this.setProgress();
  }


  selectAdditional(selected){
    if(!this.additional.includes(selected))
      this.additional.push(selected);
  }

  remove(role){
    this.additional.splice(this.additional.indexOf(role), 1);
  }

  getType(){
    return document.getElementsByClassName("typeSelected")[0].id;
  }

  getTypes(){
    const list = [];
    const c = document.getElementsByClassName("typeSelected");
    for(let i = 0; i < c.length; i++)
      list.push(c[i].id);
    return list;
  }

  goto(step){
    if(step <= this.max || this.editing !== null){
      this.step = step;
      if(this.step == 5)
        this.refreshSummary();
    }
  }

  submit(){
    const name = this.get("name").value;
    const desc = this.get("description").value;
    const hours = +this.get("hours").value;
    // let interns = +this.get("interns").value;
    // if(interns < 1) interns = 1;
    const weeks = +this.get("weeks").value;
    const location = this.get("location").value;

    const domain = this.currentUserService.currentUser.domain;
    this.firestore.collection("business_page").doc(domain).set({"domain": domain});

    const levels:number[] = [];
    const hS:string[] = [];
    if(this.hardSkills.length >= 1) {
      for(let entry of this.hardSkills.first.getSelectedList()) {
        hS.push(entry[0]);
        levels.push(0); // default value for now
      }
    }

    const businessPromise = this.http.post("https://cors-anywhere.herokuapp.com/https://addbusiness-sky44yjmca-uc.a.run.app/addBusiness", {
      "domain": domain,
      "project_name": name
    }).toPromise();

    const skillPromise = this.http.post("https://cors-anywhere.herokuapp.com/https://add-business-hard-skills-sky44yjmca-uc.a.run.app/businessHardSkillsCreate", {
      "project_name": name,
      "skills": hS,
      "levels": levels
    }).toPromise();

    const softPromise = this.http.post("https://cors-anywhere.herokuapp.com/https://add-business-soft-skills-sky44yjmca-uc.a.run.app/businessUserSoftSkillsCreate", {
      "project_name": name,
      "mainlist": this.softSkills.getRankedList()
    }).toPromise();

    // const promise = this.http.post("https://cors-anywhere.herokuapp.com/https://main-sky44yjmca-uc.a.run.app/getMatches", {
    //     "domain": domain,
    //     "project_name": name,
    //     "skills": hS,
    //     "mainlist": this.softSkills.getRankedList()
    // }).toPromise();

    this.firestore.collection("business_page").doc(domain).collection("projects").doc(name).set({
      "name": name,
      "description": desc,
      "softSkills": this.softSkills.getRankedList(),
      "role": this.get("primaryRole").value,
      "hardSkills": this.hardSkills.map(hardSkillComp => {
        const map:{[field: string]: number} = {};
        hardSkillComp.getSelectedList().forEach(skill => map[skill[0]] = skill[1]);
        return map;
      }),
      "hours": hours,
      // "interns": interns,
      "weeks": weeks,
      "location": location,
      "types": this.getTypes(),
      "selected": (this.editing == null || this.editing.selected == undefined) ? [] : this.editing.selected
    }).then(async() => {
      if(this.editing === null) {
        await businessPromise;
        await skillPromise;
        await softPromise;
      }
      await this.currentUserService.reloadProject(name);
      this.router.navigate(["dashboard/project/" + name]);
    });
  }


  onChange(target: HTMLInputElement){

    let valid = false;


    if(target.classList.contains("numberOnly"))
      target.value = target.value.replace(/[^0-9]/g, '');

    valid = target.value.length > 0;

    if(valid && target.id == "name"){
      if(this.editing == null){
        if(target.value in this.currentUserService.currentUser.company.projects){
          valid = false;
          console.log(document.getElementById("nameTaken").style.display);
          document.getElementById("nameTaken").style.display = "inline";
        }
        else
          document.getElementById("nameTaken").style.display = "none";
      }
      else target.value = this.editing.name;
    }

    if(target.id == "interns"){
      if(target.value != ""){
        let num = +target.value;
        if(num < 1) num = 1;
        if(num > 20){
          num = 20;
          target.value = "20";
        }
        this.interns = Array(num).fill(0).map((x,i)=>i+1);
      }
    }

    if(valid)
      target.classList.remove('invalid');
    else
      target.classList.add('invalid');

  }

  select(element: Element){
    // const list = document.getElementsByClassName("type");
    // for(let i = 0; i < list.length; i++)
    //   list[i].classList.remove("typeSelected");
    if(element.classList.contains("typeSelected"))
      element.classList.remove("typeSelected");
    else
      element.classList.add("typeSelected");
  }

  public clearSelect(){
    const list = document.getElementsByClassName("type");
    for(let i = 0; i < list.length; i++)
      list[i].classList.remove("typeSelected");
  }

  public scrollTo(element){
    var top = this.offsetTop(element) - ( window.innerHeight / 2 );
    window.scrollTo( 0, top );
  }

  public offsetTop(element) {
    return element.offsetTop + ( element.offsetParent ? this.offsetTop(element.offsetParent) : 0 );
  }

  public check(){
    return this.checkList(Array.from(document.getElementsByClassName("required")));
  }

  public checkStep(s:number){
    const el = document.getElementById("s_" + s);
    return this.checkList(Array.from(el.querySelectorAll(".required")));
  }

  public checkList(list: Element[]){
    let valid = true;

    for(let i = 0; i < list.length; i++){
      const el = <HTMLInputElement>list[i];

      this.onChange(el);

      if(valid && el.classList.contains("invalid")){
        this.scrollTo(el);
        valid = false;
      }

    }

    return valid;
  }

  public next(){
    if(this.checkStep(this.step)){
      this.step++;
      if(this.step > this.max) {
        this.max = this.step;
        this.setProgress();
      }
      window.scrollTo(0,0);
      if(this.step == 5)
        this.refreshSummary();
    }
  }

  refreshSummary(){
    document.getElementById("summaryHours").innerHTML = this.get("hours").value;
    document.getElementById("summaryWeeks").innerHTML = this.get("weeks").value;
    document.getElementById("summaryDesc").innerHTML = this.get("description").value;
    document.getElementById("summaryLocation").innerHTML = this.get("location").value;
    document.getElementById("summaryRole").innerHTML = this.get("primaryRole").value;

    this.summarySkills = [];
    if(this.hardSkills.length >= 1){
      for(let entry of this.hardSkills.first.getSelectedList())
        this.summarySkills.push(entry[0]);
    }
  }

  public setProgress(){
    document.getElementById("progressAfter").style.width = ((this.max+1.0) / 6.0 * 100)+"%";
  }

  public previous(){
    this.step--;
  }

}
