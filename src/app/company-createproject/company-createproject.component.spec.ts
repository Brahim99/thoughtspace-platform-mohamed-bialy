import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCreateprojectComponent } from './company-createproject.component';

describe('CompanyCreateprojectComponent', () => {
  let component: CompanyCreateprojectComponent;
  let fixture: ComponentFixture<CompanyCreateprojectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyCreateprojectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCreateprojectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
