import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkspaceWorkhistoryComponent } from './workspace-workhistory.component';

describe('WorkspaceWorkhistoryComponent', () => {
  let component: WorkspaceWorkhistoryComponent;
  let fixture: ComponentFixture<WorkspaceWorkhistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkspaceWorkhistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspaceWorkhistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
