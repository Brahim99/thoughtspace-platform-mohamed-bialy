import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkspaceStatsComponent } from './workspace-stats.component';

describe('WorkspaceStatsComponent', () => {
  let component: WorkspaceStatsComponent;
  let fixture: ComponentFixture<WorkspaceStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkspaceStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkspaceStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
