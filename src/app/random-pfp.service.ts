import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RandomPFPService {

  links:string[] = [
    "https://i.imgur.com/NbIdDKe.jpg", "https://i.imgur.com/nFkNJYy.jpg", "https://i.imgur.com/At7Bmis.jpg", "https://i.imgur.com/N2HWGin.jpg", 
    "https://i.imgur.com/S9ZwwMF.jpg", "https://i.imgur.com/fs43rx4.jpg", "https://i.imgur.com/wQx5gvC.jpg", "https://i.imgur.com/wQx5gvC.jpg", 
    "https://i.imgur.com/blQCmyk.jpg", "https://i.imgur.com/SW5srkw.jpg", "https://i.imgur.com/8mWCUqb.jpg", "https://i.imgur.com/CblBKEB.jpg", 
    "https://i.imgur.com/0ipzbzZ.jpg", "https://i.imgur.com/ot5GCZK.jpg", "https://i.imgur.com/syaUM4a.jpg", "https://i.imgur.com/aQiBPDB.jpg", 
    "https://i.imgur.com/b9aPYMi.jpg", "https://i.imgur.com/YqeJA6y.jpg", "https://i.imgur.com/0SnFz4L.jpg", "https://i.imgur.com/iTpb0dX.jpg", 
    "https://i.imgur.com/NbKlA1d.jpg", "https://i.imgur.com/3JSH9qi.jpg", "https://i.imgur.com/qJnqwnu.jpg", "https://i.imgur.com/f7aS2nQ.jpg", 
    "https://i.imgur.com/Pjfj4KJ.jpg", "https://i.imgur.com/KQTrE55.jpg", "https://i.imgur.com/goZtqvn.jpg", "https://i.imgur.com/lrTogv6.jpg", 
    "https://i.imgur.com/UAlccpX.jpg", "https://i.imgur.com/nOpoEql.jpg", "https://i.imgur.com/Hg0B4jX.jpg", "https://i.imgur.com/0yiovP2.jpg", 
    "https://i.imgur.com/lyZEByM.jpg", "https://i.imgur.com/erpdBhr.jpg", "https://i.imgur.com/sPiUoc0.jpg", "https://i.imgur.com/c3Rad7F.jpg", 
    "https://i.imgur.com/KhM7vky.jpg", "https://i.imgur.com/uf4M7NB.jpg", "https://i.imgur.com/fAO3tLg.jpg", "https://i.imgur.com/3mnokH1.jpg", 
    "https://i.imgur.com/ipAqjoE.jpg", "https://i.imgur.com/tTVTFsB.jpg", "https://i.imgur.com/gPzxMYg.jpg", "https://i.imgur.com/ELHtMIC.jpg"
  ];

  linksCool:string[] = [
    "https://i.imgur.com/ARZu6r0.jpg", "https://i.imgur.com/eJxzQkv.jpg", "https://i.imgur.com/CUQsAxY.jpg", "https://i.imgur.com/Ru5gSFB.jpg", 
    "https://i.imgur.com/ESYtzga.jpg", "https://i.imgur.com/x1Tcoxt.jpg", "https://i.imgur.com/tnHQslT.jpg", "https://i.imgur.com/8CchVNb.jpg",
    "https://i.imgur.com/hwSzgvq.jpg", "https://i.imgur.com/P9c3Egr.jpg", "https://i.imgur.com/4vQhtVc.jpg", "https://i.imgur.com/lkziU16.jpg",
    "https://i.imgur.com/ZUwmEzB.jpg", "https://i.imgur.com/QC9AmjT.jpg", "https://i.imgur.com/ekTTZ2r.jpg", "https://i.imgur.com/ITpzvX2.jpg",
    "https://i.imgur.com/EzLJss3.jpg", "https://i.imgur.com/isI4h5B.jpg", "https://i.imgur.com/aimXUL3.jpg", "https://i.imgur.com/PoMFIvX.jpg"
  ];

  constructor() { }

  getLink() : string {
    return this.links[Math.floor(Math.random() * this.links.length)];
  }

  getLinkFromEither() : string {
    if(Math.random() <= 0.2)
      return this.links[Math.floor(Math.random() * this.links.length)];
    return this.linksCool[Math.floor(Math.random() * this.linksCool.length)];
  }
  
}
