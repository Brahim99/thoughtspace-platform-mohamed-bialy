import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { CurrentUserService } from './current-user.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardGuardService implements CanActivate {

  constructor(public auth:AngularFireAuth, public router:Router, public currentUserService: CurrentUserService) { }

  async canActivate() {
    if (localStorage.getItem("uid") === null) {
      this.router.navigate(['login']);
      return false;
    }
    if(localStorage.getItem("verified") !== "true"){
        this.router.navigate(['verify']);
        return false;
    }

    if(!this.currentUserService.isReady)
      await this.currentUserService.reload();

    if(this.currentUserService.currentUser == null){
      this.router.navigate(['login']);
      return false;
    }
    
    return true;
  }

}
