import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CurrentUserService, Project } from '../current-user.service';

export class Question {
  type:number = 0;
  choices:number = 4;

  constructor(type:number) {
    this.type = type;
  }
}

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})

export class SurveyComponent implements OnInit {

  questions: Question[] = [];
  viewing: Project;

  constructor(public routed: ActivatedRoute, public router: Router, public firestore: AngularFirestore, public currentUserService: CurrentUserService) { }

  ngOnInit() {

    this.routed.url.subscribe(url => {
      if(url != null && url != undefined && url.length > 2 && url[1].path == "survey"){
        const name = url[2].path;
        
        this.currentUserService.runWhenReady(() => {
          const project = this.currentUserService.currentUser.company.projects[name];
          if(project == undefined)
            this.router.navigate(["dashboard"]);
          else this.load(project);
        });
      }
    });
      
  }

  load(project: Project){
    this.viewing = project;

    this.firestore.doc("business_page/" + this.viewing.domain + "/projects/" + this.viewing.name).get().toPromise().then(doc => {
      if(doc.exists && "survey" in doc.data()){
        this.questions = [];
        const survey = doc.data()["survey"];

        let i = 0;
        for(let q of survey){
          const question: Question = new Question(q["type"]);
          const quest = q["question"];

          const ii = i;

          if(question.type == 0){
            const choices = q["choices"];
            question.choices = choices.length;
            
            setTimeout(() => {
              const c = document.getElementsByClassName("mc_q" + ii);
              for(let ind = 0; ind < choices.length; ind++)
                (<HTMLInputElement> c[ind]).value = choices[ind];
            }, 1);
          }

          setTimeout(() => {
            (<HTMLInputElement> document.getElementById("question_" + ii)).value = quest;
          }, 1);

          this.questions.push(question);

          i++;
        }
      }
    })
  }

  addMC(){
    this.questions.push(new Question(0));
  }

  addFR(){
    this.questions.push(new Question(1));
  }

  addSC(){
    this.questions.push(new Question(2));
  }

  delete(question: Question){
    this.questions.splice(this.questions.indexOf(question), 1);
  }

  send(){
    const survey: {[field: string]: any}[] = [];
    let i = 0;
    for(let question of this.questions){
      const q: {[field: string]: any} = {};
      q["type"] = question.type;
      q["question"] = (<HTMLInputElement> document.getElementById("question_" + i)).value;
      
      if(question.type == 0){ // multiple choice
        const c:string[] = [];
        const choices = document.getElementsByClassName("mc_q" + i);
        for(let ind = 0; ind < choices.length; ind++)
          c.push((<HTMLInputElement> choices[ind]).value);
        q["choices"] = c;
      }

      survey.push(q);

      i++;
    }

    this.firestore.doc("business_page/" + this.viewing.domain + "/projects/" + this.viewing.name).update({"survey": survey});
    this.router.navigate(["dashboard/project/" + this.viewing.name]);
  }

  cancel(){
    this.router.navigate(["dashboard/project/" + this.viewing.name]);
  }

}
