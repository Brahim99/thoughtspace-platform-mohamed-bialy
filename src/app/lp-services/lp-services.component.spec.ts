import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpServicesComponent } from './lp-services.component';

describe('LpServicesComponent', () => {
  let component: LpServicesComponent;
  let fixture: ComponentFixture<LpServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
