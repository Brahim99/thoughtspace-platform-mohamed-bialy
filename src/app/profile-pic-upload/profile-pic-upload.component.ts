import { Component, OnInit, Input } from '@angular/core';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { AngularFirestore } from '@angular/fire/firestore';
import { DashboardNAVComponent } from '../dashboard-nav/dashboard-nav.component';
import { DataService } from '../data.service';
import { DashboardAccountComponent } from '../dashboard-account/dashboard-account.component';
import { RandomPFPService } from '../random-pfp.service';

@Component({
  selector: 'app-profile-pic-upload',
  templateUrl: './profile-pic-upload.component.html',
  styleUrls: ['./profile-pic-upload.component.css']
})
export class ProfilePicUploadComponent implements OnInit {

  constructor(private storage: AngularFireStorage, private db: AngularFirestore, private data: DataService, private randomPFP: RandomPFPService) { }

  @Input() dAccount:DashboardAccountComponent;

  task: AngularFireUploadTask;
  isHovering: boolean;
  open = false;

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  startUpload(event: FileList) {
    const file = event.item(0);

    if (file.type.split('/')[0] !== 'image') { 
      console.error('unsupported file type :( ')
      return;
    }

    const uid = localStorage.getItem("uid");

    const path = "files/" + uid + "/profile." + file.name.substr(file.name.lastIndexOf(".") + 1);

    const cPath = localStorage.getItem("intern") === "true" ? "intern_users/" : "business_users/";

    this.storage.upload(path, file).then(() => {
      this.storage.ref(path).getDownloadURL().toPromise().then(url => {
        this.db.doc(cPath + uid + "/profile/main").update({"profilePic": url}).then(() => {
          this.open = false;
          this.data.pfpPath = url;
          this.data.cached.pfpPath = url;
          this.data.updateCache();
        });
      });
    });
  }

  rando(){
    const url = this.randomPFP.getLinkFromEither();

    const uid = localStorage.getItem("uid");
    const cPath = localStorage.getItem("intern") === "true" ? "intern_users/" : "business_users/";

    this.db.doc(cPath + uid + "/profile/main").update({"profilePic": url}).then(() => {
      // this.open = false;
      this.data.pfpPath = url;
      this.data.cached.pfpPath = url;
      this.data.updateCache();
    });
  }

  ngOnInit() {

    this.dAccount.ppUpload = this;

    window.onkeydown = (ev) => {
      if(this.open && ev.keyCode === 27)
        this.open = false;
    };
  }

}
