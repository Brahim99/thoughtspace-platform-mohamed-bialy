import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LpPlatformFeaturesComponent } from './lp-platform-features.component';

describe('LpPlatformFeaturesComponent', () => {
  let component: LpPlatformFeaturesComponent;
  let fixture: ComponentFixture<LpPlatformFeaturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LpPlatformFeaturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LpPlatformFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
