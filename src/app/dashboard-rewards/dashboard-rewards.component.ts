import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../current-user.service';

@Component({
  selector: 'app-dashboard-rewards',
  templateUrl: './dashboard-rewards.component.html',
  styleUrls: ['./dashboard-rewards.component.css']
})
export class DashboardRewardsComponent implements OnInit {

  constructor(public currentUserService:CurrentUserService) { }

  ngOnInit() {
  }

}
