import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRewardsComponent } from './dashboard-rewards.component';

describe('DashboardRewardsComponent', () => {
  let component: DashboardRewardsComponent;
  let fixture: ComponentFixture<DashboardRewardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardRewardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRewardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
