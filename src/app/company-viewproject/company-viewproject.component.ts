import { Component, OnInit, ViewChild, Input, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router, ActivatedRoute } from '@angular/router';
import { Project, CurrentUserService } from '../current-user.service';
import { HttpClient } from '@angular/common/http';
import { MatSliderChange } from '@angular/material';
import { FeedbackComponent } from '../feedback/feedback.component';
import {Label, MultiDataSet} from "ng2-charts";

@Component({
  selector: 'app-company-viewproject',
  templateUrl: './company-viewproject.component.html',
  styleUrls: ['./company-viewproject.component.css']
})
export class CompanyViewprojectComponent implements OnInit {

  @Input() feedbackComponent: FeedbackComponent;

  private starred:string[] = [];
  public view:string = "all";

  public viewing: Project = null;
  public matches: {[field: string]: {[field: string]: any}} = null;
  public shownMatches: string[] = null;
  public postOpen: boolean = false;
  public videoOpen: boolean = false;

  constructor(public firestore: AngularFirestore, public router: Router, public routed: ActivatedRoute, public currentUserService: CurrentUserService, private http:HttpClient) { }

  public ssLabels: Label[] = ["A", "B"];
  public ssColors = [
    { // grey
      backgroundColor: [
        'rgba(0, 155, 255,0.8)',
        'rgba(0,0,0,0)'
      ],
      borderColor: [
        'rgba(148,159,177,1)',
        'rgba(0,0,0,0)'
      ],
      borderWidth: 0
    }
  ];

  ngOnInit() {
    this.routed.url.subscribe(url => {
      if(url != null && url != undefined && url.length > 2 && url[1].path == "project"){
        const name = url[2].path;

        this.currentUserService.runWhenReady(() => {
          const project = this.currentUserService.currentUser.company.projects[name];
          if(project == undefined)
            this.router.navigate(["dashboard"]);
          else
            this.load(project);
        });
      }
    });
  }

  edit(){
    this.router.navigate(["dashboard/edit/" + this.viewing.name]);
  }

  updateShown(){

    if(this.matches === null || this.matches === undefined)
      this.shownMatches = null;
    else {
      this.shownMatches = [];
      for(let intern in this.matches)
        if(this.viewing.selected === undefined || !this.viewing.selected.includes(intern))
          this.shownMatches.push(intern);
    }
  }

  isMatch(intern:string){
    return intern in this.matches;
  }

  async load(project:Project){
    this.viewing = project;

    this.matches = project.matches;
    this.updateShown();

    if(this.matches === null || this.matches === undefined || Object.keys(this.matches).length == 0)
      this.refreshMatches();
  }

  refreshMatches(){
    this.shownMatches = [];
    this.viewing.matches = this.matches = {};

    const project = this.viewing;

    const hardSkills:string[] = [];

    if(project.hardSkills.length < 1) return;
    for(let key in project.hardSkills[0])
      hardSkills.push(key);

    this.http.post("https://cors-anywhere.herokuapp.com/https://algorithm-sky44yjmca-uc.a.run.app/getMatches", {
      "project_name": project.name,
      "skills": hardSkills
    }).toPromise().then(async(resp) => {

      const array = <Array<any>> resp;
      for(const batch of array){
        const skills = [];

        for(const skill of batch["batch_data"]["skills"])
          skills.push(skill);
        const results = batch["batch_data"]["uids"]["results"];
        for(let i = 0; i < results.length; i++){
          const uin = results[i]["uid"];
          const docs = await this.firestore.collection("Clariant/results/students").ref.where("uin", "==", uin).get();
          if(docs.docs.length > 0){
            const data = docs.docs[0].data();
            let score = Math.trunc(results[i]["percentage"] * 100);
            if(score < 0) score = 0;

            this.matches[uin] = {
              "shortname": data["firstname"] + " " + data["lastname"][0]+".",
              "sharedskills": skills,
              "score": score,
              "data": data
            };
          }
        }
      }

      if(Object.keys(this.matches).length > 0)
        this.firestore.doc("business_page/" + project.domain + "/projects/" + project.name).update({"matches" : this.matches});
      this.viewing.matches = this.matches;

      this.updateShown();

    }).catch(err => {
      console.log("ERR " + err);
    });
  }

  hasMatches(){
    return this.shownMatches !== null && this.shownMatches !== undefined && Object.keys(this.shownMatches).length > 0;
  }

  clear(element: HTMLInputElement){
    element.value = "";
    element.classList.remove("invalid");
  }

  survey(){
    this.router.navigate(["dashboard/survey/" + this.viewing.name]);
  }

  reset(){
    this.viewing = null;
    this.shownMatches = this.matches = null;
  }

  reject(intern:string){
    this.shownMatches.splice(this.shownMatches.indexOf(intern), 1);
  }

  select(intern:string){
    const ind = this.viewing.selected === undefined ? -1 : this.viewing.selected.indexOf(intern);
    if(ind == -1) {
      if(this.viewing.selected === undefined)
        this.viewing.selected = [];
      this.viewing.selected.push(intern);
      this.shownMatches.splice(this.shownMatches.indexOf(intern), 1);
    }
    else {
      this.viewing.selected.splice(ind, 1);
      this.shownMatches.push(intern);
    }

    const doc = this.firestore.doc("business_page/" + this.viewing.domain + "/projects/" + this.viewing.name).update({"selected" : this.viewing.selected});
  }

  evaluate(intern:string){
    console.log(this.viewing);
    console.log(this.viewing.matches);
    if(intern in this.viewing.matches)
      this.router.navigate(["dashboard/evaluate/" + this.viewing.name + "/" + intern]);
  }

  moreinfo(clicked:HTMLElement){
    if(!clicked.classList.contains("add-br")) return;
    const el = clicked.childNodes[1] as HTMLElement;
    el.style.display = (el.style.display === "none" || el.style.display === "") ? "block" : "none";
  }

  swapStar(intern){
    if(this.starred.includes(intern))
      this.starred.splice(this.starred.indexOf(intern), 1);
    else this.starred.push(intern);
    // if(clicked.innerHTML === "☆"){
    //   clicked.innerHTML = "★";
    //   clicked.style.color = "#009BFF";
    // }
    // else {
    //   clicked.innerHTML = "☆";
    //   clicked.style.color = "#DBDBDB";
    // }
  }

  post(){
    this.postOpen = !this.postOpen;
  }

  video(){
    this.videoOpen = !this.videoOpen;
  }

  containerClick(clicked:HTMLElement){
    if(clicked.classList.contains("backgroundShader") || clicked.classList.contains("popupBox")) return;
    const els = document.getElementsByClassName("moreinfo");
    for(let i = 0; i < els.length; i++){
      if(!els[i].parentElement.contains(clicked))
        (els[i] as HTMLElement).style.display = "none";
    }
  }

  matchesShown(){
    if(this.view !== 'starred') return this.shownMatches;
    const shown = [];
    for(let match of this.shownMatches)
      if(this.starred.includes(match))
        shown.push(match);
    return shown;
  }
}
