import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyViewprojectComponent } from './company-viewproject.component';

describe('CompanyViewprojectComponent', () => {
  let component: CompanyViewprojectComponent;
  let fixture: ComponentFixture<CompanyViewprojectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyViewprojectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyViewprojectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
