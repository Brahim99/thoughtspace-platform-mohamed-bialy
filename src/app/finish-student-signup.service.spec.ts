import { TestBed } from '@angular/core/testing';

import { FinishStudentSignupService } from './finish-student-signup.service';

describe('FinishStudentSignupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FinishStudentSignupService = TestBed.get(FinishStudentSignupService);
    expect(service).toBeTruthy();
  });
});
