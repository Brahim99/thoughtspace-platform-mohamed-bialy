import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { RandomPFPService } from '../random-pfp.service';
import { VerifyService } from '../verify.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-basic-info-career',
  templateUrl: './basic-info-career.component.html',
  styleUrls: ['./basic-info-career.component.css']
})
export class BasicInfoCareerComponent implements OnInit {

  constructor(public router:Router, public firestore:AngularFirestore, public randomPFP: RandomPFPService, public verifier: VerifyService, private auth: AngularFireAuth) { }

  selected:string = null;

  select(event){
    
    const list = document.getElementsByClassName("option");
    for(let i = 0; i < list.length; i++)
      list[i].classList.remove("selected");

    event.target.classList.add("selected");
    this.selected = event.target.id;

  }

  ngOnInit() {
  }

  finish(){
    
  }

}
