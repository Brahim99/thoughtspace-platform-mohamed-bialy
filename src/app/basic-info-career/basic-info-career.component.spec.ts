import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicInfoCareerComponent } from './basic-info-career.component';

describe('BasicInfoCareerComponent', () => {
  let component: BasicInfoCareerComponent;
  let fixture: ComponentFixture<BasicInfoCareerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInfoCareerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfoCareerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
