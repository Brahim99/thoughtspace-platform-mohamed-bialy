import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPracticeComponent } from './dashboard-practice.component';

describe('DashboardPracticeComponent', () => {
  let component: DashboardPracticeComponent;
  let fixture: ComponentFixture<DashboardPracticeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPracticeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPracticeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
