import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  constructor() { }

  @Input() append:boolean = false;

  isHovering: boolean;
  chosen: File[] = [];

  toggleHover(event: boolean) {
    this.isHovering = event;
  }

  startUpload(event: FileList) {

    if(!this.append)
      this.chosen = [];
    for(let i = 0; i < event.length; i++)
      this.chosen.push(event.item(i));

    let s = "";

    if(event.length == 0)
      s = "No file chosen."
    else {
      for(let i = 0; i < this.chosen.length; i++)
        s += (s == "" ? "" : ", ") + this.chosen[i].name;
    }

    document.getElementById("chosen").innerHTML = s;

  }

  ngOnInit(){

  }

}
