import { TestBed } from '@angular/core/testing';

import { VerifyGuardService } from './verify-guard.service';

describe('VerifyGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VerifyGuardService = TestBed.get(VerifyGuardService);
    expect(service).toBeTruthy();
  });
});
