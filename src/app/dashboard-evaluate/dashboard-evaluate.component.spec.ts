import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEvaluateComponent } from './dashboard-evaluate.component';

describe('DashboardEvaluateComponent', () => {
  let component: DashboardEvaluateComponent;
  let fixture: ComponentFixture<DashboardEvaluateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardEvaluateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
