import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-evaluate',
  templateUrl: './dashboard-evaluate.component.html',
  styleUrls: ['./dashboard-evaluate.component.css']
})
export class DashboardEvaluateComponent implements OnInit {

  constructor(public router:Router) { }

  ngOnInit() {
  }

  cancel(){
    this.router.navigate(['dashboard']);
  }

  submit(){
    this.cancel();
  }

  onSlide(e){
    
  }
}
