import { Component, OnInit, Input } from '@angular/core';
import { DashboardViewComponent } from '../dashboard-view/dashboard-view.component';
import { Router } from '@angular/router';
import { CurrentUserService } from '../current-user.service';

@Component({
  selector: 'app-dashboard-menu',
  templateUrl: './dashboard-menu.component.html',
  styleUrls: ['./dashboard-menu.component.css']
})
export class DashboardMenuComponent implements OnInit {

  @Input() dView: DashboardViewComponent;

  constructor(public router: Router, public currentUserService: CurrentUserService) { }

  ngOnInit() {
    this.currentUserService.runWhenReady(() => console.log(this.currentUserService.projects));
  }

  keydown(e){
    this.dView.onSearchKeydown(e);
  }

  keyup(e){
    this.dView.onSearchKeyup(e);
  }

}
