import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color, MultiDataSet, SingleDataSet } from 'ng2-charts';
import { CurrentUserService } from '../current-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-analytics',
  templateUrl: './company-analytics.component.html',
  styleUrls: ['./company-analytics.component.css']
})
export class CompanyAnalyticsComponent implements OnInit {

  public view:string = "all";

  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['Intern A', 'Intern B', 'Intern C', 'Intern D', 'Intern E', 'Intern F', 'Intern G'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;

  public barChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Tasks Completed' },
    { data: [28, 48, 40, 19, 86, 27, 90], label: 'Hours Worked' }
  ];

  public hoursWorkedData: ChartDataSets[];
  public hoursWorkedLabels: Label[];
  public hoursWorkedOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    annotation: {annotations: []},
  };
  public hoursWorkedColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(255,255,255,0.2)',
      borderColor: 'rgba(0,178,255,1)',
      pointBackgroundColor: 'rgba(0,104,255,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  public tasksData: ChartDataSets[];
  public tasksLabels: Label[];
  public tasksOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    annotation: {annotations: []},
  };
  public tasksColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  public hardData: ChartDataSets[];
  public hardLabels: Label[];
  public hardOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    annotation: {annotations: []},
  };
  public hardColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      borderWidth: 1
    }
  ];

  public softData: ChartDataSets[];
  public softLabels: Label[];
  public softOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
    scales: {
      xAxes: [{}],
      yAxes: [
        {
          id: 'y-axis-0',
          position: 'left',
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
    annotation: {annotations: []},
  };
  public softColors: Color[] = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      borderWidth: 1
    }
  ];

  public engageData: SingleDataSet;
  public engageLabels: Label[];
  public engageColors = [
    { // grey
      backgroundColor: 'rgba(255,185,0,0.2)',
      borderColor: 'rgba(225,185,0,1)',
      borderWidth: 1
    }
  ];

  public roiData: MultiDataSet = [[65, 35]];
  public roiLabels: Label[] = ["A", "B"];
  public roiColors = [
    { // grey
      backgroundColor: [
        'rgba(148,159,177,0.2)',
        'rgba(0,0,0,0)'
      ],
      borderColor: [
        'rgba(148,159,177,1)',
        'rgba(0,0,0,0)'
      ],
      borderWidth: 1
    }
  ];

  constructor(public currentUserService: CurrentUserService, public router:Router) { }

  ngOnInit() {
    this.setHoursDay();
    this.setTasksDay();
    this.setHardDay();
    this.setSoftDay();
    this.setEngageDay();
  }

  setHoursDay(){
    this.hoursWorkedData = [{ data: [4, 2, 6, 3, 4, 3, 5], lineTension: 0 }];
    this.hoursWorkedLabels = ['9/2', '9/3', '9/4', '9/5', '9/6', '9/7', '9/8'];
  }

  setHoursWeek(){
    this.hoursWorkedData = [{ data: [27, 28, 26, 25, 23, 26, 30], lineTension: 0 }];
    this.hoursWorkedLabels = ['7/28', '8/4', '8/11', '8/18', '8/25', '9/1', '9/8'];
  }

  setHoursMonth(){
    this.hoursWorkedData = [{ data: [84, 77, 65, 59, 80, 81, 56, 55, 40], lineTension: 0 }];
    this.hoursWorkedLabels = ["January", "February", 'March', 'April', 'May', 'June', 'July', 'August', 'September'];
  }

  setTasksDay(){
    this.tasksData = [{ data: [2, 1, 1, 4, 3, 6, 7], lineTension: 0 }];
    this.tasksLabels = ['9/2', '9/3', '9/4', '9/5', '9/6', '9/7', '9/8'];
  }

  setTasksWeek(){
    this.tasksData = [{ data: [10, 14, 10, 8, 9, 12, 8], lineTension: 0 }];
    this.tasksLabels = ['7/28', '8/4', '8/11', '8/18', '8/25', '9/1', '9/8'];
  }

  setTasksMonth(){
    this.tasksData = [{ data: [40, 42, 44, 54, 53, 49, 52, 55, 56], lineTension: 0 }];
    this.tasksLabels = ["January", "February", 'March', 'April', 'May', 'June', 'July', 'August', 'September'];
  }

  setHardDay(){
    this.hardData = [{ data: [2, 1, 1, 4, 3, 6, 7], lineTension: 0 }];
    this.hardLabels = ['9/2', '9/3', '9/4', '9/5', '9/6', '9/7', '9/8'];
  }

  setHardWeek(){
    this.hardData = [{ data: [10, 14, 10, 8, 9, 12, 8], lineTension: 0 }];
    this.hardLabels = ['7/28', '8/4', '8/11', '8/18', '8/25', '9/1', '9/8'];
  }

  setHardMonth(){
    this.hardData = [{ data: [40, 42, 44, 54, 53, 49, 52, 55, 56], lineTension: 0 }];
    this.hardLabels = ["January", "February", 'March', 'April', 'May', 'June', 'July', 'August', 'September'];
  }

  setSoftDay(){
    this.softData = [{ data: [1, 2, 1, 3, 6, 7, 8], lineTension: 0 }];
    this.softLabels = ['9/2', '9/3', '9/4', '9/5', '9/6', '9/7', '9/8'];
  }

  setSoftWeek(){
    this.softData = [{ data: [13, 11, 8, 10, 9, 8, 12], lineTension: 0 }];
    this.softLabels = ['7/28', '8/4', '8/11', '8/18', '8/25', '9/1', '9/8'];
  }

  setSoftMonth(){
    this.softData = [{ data: [44, 40, 44, 49, 53, 49, 55, 52, 56], lineTension: 0 }];
    this.softLabels = ["January", "February", 'March', 'April', 'May', 'June', 'July', 'August', 'September'];
  }

  setEngageDay(){
    this.engageData = [42, 28, 35, 39, 25, 43, 34];
    this.engageLabels = ['9/2', '9/3', '9/4', '9/5', '9/6', '9/7', '9/8'];
  }

  setEngageWeek(){
    this.engageData = [42, 35, 25, 39, 33, 28, 34];
    this.engageLabels = ['7/28', '8/4', '8/11', '8/18', '8/25', '9/1', '9/8'];
  }

  setEngageMonth(){
    this.engageData = [28, 32, 35, 28, 25, 43, 33, 38, 34];
    this.engageLabels = ["January", "February", 'March', 'April', 'May', 'June', 'July', 'August', 'September'];
  }

  select(project){
    this.view = project;

    this.hoursWorkedData = [{ data: [this.r(4), this.r(2), this.r(6), this.r(3), this.r(4), this.r(3), this.r(5)], lineTension: 0 }];
    this.hoursWorkedLabels = ['9/2', '9/3', '9/4', '9/5', '9/6', '9/7', '9/8'];

    this.engageData = [this.r(42), this.r(28), this.r(35), this.r(39), this.r(25), this.r(43), this.r(34)];
    this.engageLabels = ['9/2', '9/3', '9/4', '9/5', '9/6', '9/7', '9/8'];

    this.tasksData = [{ data: [this.r(2), this.r(1), this.r(1), this.r(4), this.r(3), this.r(6), this.r(7)], lineTension: 0 }];
    this.tasksLabels = ['9/2', '9/3', '9/4', '9/5', '9/6', '9/7', '9/8'];
  }

  r(r){
    return Math.floor(Math.random() * r);
  }
}
