import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { SearchComponent } from '../search/search.component';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardAccountComponent } from '../dashboard-account/dashboard-account.component';
import { CurrentUserService, Project } from '../current-user.service';

@Component({
  selector: 'app-dashboard-view',
  templateUrl: './dashboard-view.component.html',
  styleUrls: ['./dashboard-view.component.css']
})
export class DashboardViewComponent implements OnInit {

  constructor(public routed:ActivatedRoute, public router:Router, public currentUserService: CurrentUserService) { }

  @ViewChild('search') search: SearchComponent;
  @ViewChild('account') dAccount: DashboardAccountComponent;

  view:string = "";

  accept(project: Project){
    project.accepted = true;
    this.router.navigate(["dashboard/project/" + project.name]);
  }

  ngOnInit() {

    const frag = this.routed.snapshot.fragment;
    if(frag !== null && frag !== undefined)
      this.view = frag;

    this.routed.fragment.subscribe(fragment => {
      const url = this.routed.snapshot.url;
      if(url.length > 2 && url[1].path == "project"){
        this.view = "workspace";
        return;
      }

      if(url.length == 2)
        this.view = url[1].path;
      else if(fragment !== null && fragment !== undefined)
        this.view = fragment
      else
        this.view = "";
    });
  }

  onSearchKeydown(e){
  }

  onSearchKeyup(e){
    this.view = "search";
    this.search.go(e.target.value);
  }

}
