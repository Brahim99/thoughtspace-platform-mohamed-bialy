import { Component, OnInit } from '@angular/core';
import { SignupService } from '../signup.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(public signupService:SignupService) { }

  jump(state){
    this.signupService.jump(state);
  }

  ngOnInit() {
  }

}
