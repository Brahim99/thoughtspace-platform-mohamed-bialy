import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { RandomPFPService } from './random-pfp.service';
import { VerifyService } from './verify.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FinishStudentSignupService {

  constructor(public auth: AngularFireAuth, public firestore: AngularFirestore, public randomPFP: RandomPFPService, public verifier: VerifyService, public router:Router) { }

  populate(...args: string[]){
    const map:{[field: string]: any} = {};

    for(const arg of args)
      map[arg] = (<HTMLInputElement>document.getElementById(arg)).value;
    
    return map;
  }

  pushing:boolean = false;

  finish(){

    if(this.pushing)
      return;

    this.pushing = true;

    const email = (<HTMLInputElement>document.getElementById("email")).value;
    const pass = (<HTMLInputElement>document.getElementById("password")).value;
    this.auth.auth.createUserWithEmailAndPassword(email, pass).then(async(cred) => {

      const uid = cred.user.uid;
      localStorage.setItem("uid", uid);
      localStorage.setItem("intern", "true");
      
      const map:{[field: string]: any} = this.populate("email", "firstname", "lastname", "username", "phone");
      map["typeFlag"] = "i";
      map["uid"] = uid;
      map["phone"] = map["phone"].replace(/[^0-9]/g, '');
      map["dateCreated"] = Date.now();
      map["dateToDelete"] = (Date.now() + 30 * 24 * 60 * 60);
      map["domain"] = this.getDomain(map["email"]);
      // map["major"] = document.getElementById("major").innerHTML;
      // map["year"] = document.getElementById("year").innerHTML;
      // map["careerPath"] = this.selected;
      
      let desc = "Hello! My name is " + map['firstname'] + ".";

      const uni = await this.getUniversity(map["domain"]);
      if(uni !== null && uni !== undefined){
        map["university"] = uni;
        // desc += " I am currently studying " + map["major"] + " at " + uni + ".";
        desc += " I am currently a student at " + uni + ".";
      }

      this.firestore.doc("pending_users/" + uid).set(map);
      this.firestore.doc("pending_users/" + uid + "/profile/main").set(
        {
          "description": desc,
          "profilePic": this.randomPFP.getLink()
        }
      );
      // cred.user.sendEmailVerification();

      this.verifier.sendVerification(this.auth.auth.currentUser.uid);

      this.router.navigate(["verify"]);

    }).catch(err => {
      this.pushing = false;
    });
  }

  formatPhone(tel:string){
    let raw = tel.replace(/[^0-9]/g, '');
    
    let formatted = "";

    if(raw.length > 0){
      formatted = "(";
      if(raw.length > 3){
        formatted += raw.slice(0, 3) + ") ";

        if(raw.length > 6)
          formatted += raw.slice(3, 6) + "-" + raw.slice(6, 10);
        else
          formatted += raw.slice(3);
      }
      else
        formatted += raw;
    }

    return formatted;
  }

  getDomain(email: string){
    const start = email.indexOf("@") + 1;
    if(start <= 0) return "";
    const end = email.indexOf(".edu");
    if(end <= 0) return "";
    let id = email.substring(start, end).toLowerCase();

    const notherDot = id.lastIndexOf(".");
    if(notherDot > -1)
      id = id.substring(notherDot + 1);
    return id;
  }

  async getUniversity(domain:string){
    const matches = await this.firestore.collection("UniversityDomainSearchIndex").ref.where("domain", "==", domain).get();
    if(matches.docs.length > 0)
      return matches.docs[0].data()['university'];
    return undefined;
  }
}
