import { TestBed } from '@angular/core/testing';

import { TaskTimerService } from './task-timer.service';

describe('TaskTimerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TaskTimerService = TestBed.get(TaskTimerService);
    expect(service).toBeTruthy();
  });
});
