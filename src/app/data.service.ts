import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { BusinessIndexService } from './business-index.service';

interface Data {
  uid:string;
  mainInfo:{[field: string]: any};
  skillsInfo:{[field: string]: any};
  university:string;
  company:string;
  pfpPath:string;
  intern:boolean;
}

@Injectable({
  providedIn: 'root'
})

export class DataService {

  public mainInfo:{[field: string]: any} = {};
  public skillsInfo:{[field: string]: any} = {};
  public university:string;
  public company:string;
  public pfpPath:string;
  public intern = false;

  public needsReload = false;

  public notifier:EventEmitter<void> = new EventEmitter();
  public alreadyReady = false;

  public cached:Data;
  private lastRefresh:number;

  constructor(public firestore: AngularFirestore, public router:Router, public businessIndex: BusinessIndexService) {

    this.cached = JSON.parse(localStorage.getItem("dashboardCache"));
    this.lastRefresh = JSON.parse(localStorage.getItem("dashboardCacheLastRef"));
    this.reload();

  }

  async reload(){

    this.needsReload = false;

    const uid = localStorage.getItem("uid");

    if(uid === null || localStorage.getItem("verified") !== "true"){
      this.router.navigate(['login']);
      return;
    }

    const time = Date.now();
    if(this.cached !== null && (time - this.lastRefresh) <= 5 * 60 * 1000 && this.cached.uid === uid){
      this.mainInfo = this.cached.mainInfo;
      this.skillsInfo = this.cached.skillsInfo;
      this.university = this.cached.university;
      this.company = this.cached.company;
      this.pfpPath = this.cached.pfpPath;
      this.intern = this.cached.intern;
      localStorage.setItem("intern", this.intern + "");
    }
    else {

      let docs = await this.firestore.collection("intern_users").ref.where("uid", "==", uid).get();
      this.intern = docs.docs.length > 0;
      if(!this.intern)
        docs = await this.firestore.collection("business_users").ref.where("uid", "==", uid).get();
      
      localStorage.setItem("intern", this.intern + "");

      if(docs.docs.length > 0){
        const doc = docs.docs[0];
        this.mainInfo = doc.data();

        if(this.intern)
          this.university = this.mainInfo['university'];
        else
          await this.setCompany();

        const profile = await doc.ref.collection("profile").doc("main").get();
        this.skillsInfo = profile.data();

        this.pfpPath = this.skillsInfo['profilePic'];
      }

      this.cached = {
        uid: uid,
        mainInfo: this.mainInfo,
        skillsInfo: this.skillsInfo,
        university: this.university,
        company: this.company,
        pfpPath: this.pfpPath,
        intern: this.intern
      };

      this.lastRefresh = time;

      localStorage.setItem("dashboardCache", JSON.stringify(this.cached));
      localStorage.setItem("dashboardCacheLastRef", this.lastRefresh+"");

    }

    this.notifier.emit();
    this.alreadyReady = true;
  }

  async setCompany(){
    const domain = this.mainInfo["domain"];
    this.company = (await this.businessIndex.getIndex())[domain];
  }

  updateDesc(val:string){
    const uid = localStorage.getItem("uid");
    if(uid === undefined || uid === null){
      this.router.navigate(["login"]);
      return;
    }

    const path = (this.intern ? "intern_users/" : "business_users/") + uid;
    this.firestore.doc(path + "/profile/main").update({"description": val});
    this.skillsInfo['description'] = val;
    this.cached.skillsInfo = this.skillsInfo;
    this.updateCache();
  }

  updateCache(){
    localStorage.setItem("dashboardCache", JSON.stringify(this.cached));
  }

}
