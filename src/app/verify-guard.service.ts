import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class VerifyGuardService implements CanActivate {

  constructor(public auth:AngularFireAuth, public router:Router) { }

  canActivate(): boolean {
    this.auth.auth.onAuthStateChanged(user => {
      if(user)
        localStorage.setItem("verified", user.emailVerified+"");
    });
    if (localStorage.getItem("uid") === null) {
      this.router.navigate(['login']);
      return false;
    }
    if(localStorage.getItem("verified") === "true"){
        this.router.navigate(['dashboard']);
        return false;
    }
    return true;
  }

}
