import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { MatAutocompleteSelectedEvent, MatSliderChange, MatSlider } from '@angular/material';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Project } from '../current-user.service';

export interface Group {
  category: string;
  skills: string[];
  filteredSkills: Observable<string[]>;
}

@Component({
  selector: 'app-hard-skills',
  templateUrl: './hard-skills.component.html',
  styleUrls: ['./hard-skills.component.css']
})
export class HardSkillsComponent implements OnInit {

  @Input() id:number = 0;
  @Input() company:boolean = false;
  @Input() editing:Project = null;
  
  intForm: FormGroup;
  intControl = new FormControl();
  ready = false;
  groups: Group[] = [
    {
      category: "Operations and UI/UX",
      skills: ["Jira", "Microsoft Excel", "Project Management", "Adobe Photoshop", "Sketch", "Affinity", "Figma", "Adobe Illustrator", "Adobe XD", "Balsamiq", "Axure", "Flowmapp"],
      filteredSkills: null
    },
    {
      category: "Back End",
      skills: ["C", "C++", "Java", "Python", "PHP", "Hack", "HHVM", "Node.js", "Perl", "Ruby", "C#", "Erlang", "Go", "XHP", "Haskell", "Redis", "MongoDB", "PostgreSQL", "MySQL", "Fortran", "Firebase"],
      filteredSkills: null
    },
    {
      category: "Front End",
      skills: ["AJAX", "ASP.NET", "CSS", "HTML", "Javascript", "jQuery", "Typescript"],
      filteredSkills: null
    },
    {
      category: "Web Frameworks",
      skills: ["Angular", "Backbone", "Bootstrap", "CodeIgniter", "Django", "Ember", "Express", "Flask", "jQuery", "Laravel ", "Laravel ", "React", "Ruby on Rails", "Spring", "Symphony", "Vue"],
      filteredSkills: null
    },
    {
      category: "Marketing",
      skills: ["Adobe Premiere Pro CC", "Ahrefs", "Canva", "Chrome Eyedropper", "Crazy Egg", "Creative Cloud", "Drift (AI Chat)", "Feathr", "Gainsight", "Google Analytics", "Google console", "Google Pagespeed Insights", "Google Tag Manager", "Google URL Builder", "Grammarly", "GTMetrix (site loading)", "Hootsuite", "Hubspot", "inDesign", "Intercom", "inVision", "Lastpass", "LSI Graph", "Mail Chimp", "Optimizely", "Owler", "Piktochart", "Pixlr", "Premier", "Principle", "Quicksprout", "Salesforce", "Screaming Frog", "Siftrock ", "Subscribers", "Webflow"],
      filteredSkills: null
    },
    {
      category: "General Functions",
      skills: ["Autosum Function", "AVERAGE", "COUNT", "COUNTA", "IF Functions", "MAX & MIN", "SUM", "SWOT Analysis", "TRIM"],
      filteredSkills: null
    },
    {
      category: "Analytics",
      skills: ["Conditional Formatting", "Heptalysis", "MOST Analysis", "PEST Analysis", "Pivot Tables", "SCRS Analysis", "SWOT Analysis", "VPEC-T Analysis"],
      filteredSkills: null
    },
    {
      category: "Excel Functions",
      skills: ["CHOOSE (Financial modeling)", "Combination charts - (Dynamic, Rolling chart)", "COUNTIF", "DB - Depreciation", "FV - Future Value", "IF Formulas", "INDEX MATCH", "INTRATE -Interst rate", "LEN", "PMT and IPMT", "SUMIF", "Text-Formulas", "VLOOKUP", "XNPV and XIRR", "YIELD"],
      filteredSkills: null
    },
    {
      category: "Other",
      skills: ["A/B Testing", "BitBucket", "Command Line", "Git", "GitHub", "Grammarly", "Microsoft Powerpoint", "Microsoft Word", "Research databases", "Technical Writing", "Unit Testing", "Unity", "Unreal Engine 4", "Unix"],
      filteredSkills: null
    }
  ];

  filteredGroups: Observable<Group[]>;
  needUpdate:boolean = false;

  displayNull(value) {
    return null;
  }

  constructor() { }

  hardSkills:any[][] = [];
  hardValues:{[field: string]: number} = {};

  dropSkill(event: CdkDragDrop<any[][]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      const dropped = event.previousContainer.data[event.previousIndex];
      
      const alreadyInd = event.container.data.findIndex(v => v[0] == dropped[0]);
      let newInd = event.currentIndex;

      if(alreadyInd > -1){ // overwrite if already in target list
        if(alreadyInd <= newInd)
          newInd++;
        event.container.data.splice(alreadyInd, 1);
      }

      // transferArrayItem(event.previousContainer.data,
      //                   event.container.data,
      //                   event.previousIndex,
      //                   newInd);
      event.container.data.splice(newInd, 0, dropped); // copy instead of moving

      setTimeout(() => {
        // because the slider resets for some dumb reason
        this.updateHardSkill(event.container.element.nativeElement.id, dropped[0], dropped[1]);
      }, 1);
    }
  }

  replace(s, oldS, newS){
    return s.split(oldS).join(newS);
  }

  updateHardSkill(id:string, name:string, value:number){
    name = this.replace(name, ' ', '_');
    const el = document.getElementById(id + "_" + name);
    if(el.children[1] === undefined) return;
    el.children[1].innerHTML = "Proficiency: " + value;
    
    const wr = el.children[2].children[0]; // mat slider wrapper
    const left = value * 10; // distance from left
    const right = (10 - value) * 10; // distance from right
    (<HTMLElement> wr.children[0].children[0]).style.transform = "translateX(0px) scale3d(" + right/100 + ", 1, 1)";
    (<HTMLElement> wr.children[0].children[1]).style.transform = "translateX(0px) scale3d(" + left/100 + ", 1, 1)";
    (<HTMLElement> wr.children[2]).style.transform = "translateX(-" + right + "%)";
  }

  onSlide(event: MatSliderChange){
    const element = <HTMLElement> event.source._elementRef.nativeElement;
    const display = <HTMLElement> (element.parentElement.childNodes[1]);
    display.innerHTML = "Proficiency: " + event.value;
    for(let s of this.hardSkills){
      if(this.replace(s[0], ' ', '_') == display.classList[0]){
        s[1] = event.value;
        break;
      }
    }
  }

  onSelect(event: MatAutocompleteSelectedEvent){
    const index = this.hardSkills.findIndex(v => v[0] == event.option.value);
    if(index > -1)
    this.hardSkills.splice(index, 1);
    else
      this.hardSkills.push([event.option.value, 5]);

    event.option.deselect();
    this.intControl.setValue('');

    const c = document.getElementsByClassName("hfield");
    for(let i = 0; i < c.length; i++)
      (<HTMLElement> c[i]).blur();
  }

  remove(skill:string){
    const index = this.hardSkills.findIndex(v => v[0] == skill);
    if(index > -1)
      this.hardSkills.splice(index, 1);
  }

  ngOnInit() {

    if(this.editing != null && this.editing != undefined && this.id - 1 < this.editing.hardSkills.length)
      this.setSelectedList(this.editing.hardSkills[this.id - 1]);

    this.intForm = new FormGroup({'intControl': this.intControl});

    for(let group of this.groups){
      group.filteredSkills = this.intControl.valueChanges
        .pipe(
          startWith(''),
          map(value => this._filter(group.skills, value))
        );
    }

    this.filteredGroups = this.intControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this.groups.filter(group => this._filter(group.skills, value).length > 0))
      );

    this.ready = true;
  }

  getSelectedList(){
    return this.hardSkills;
  }

  setSelectedList(list: {[field: string]: number}){
    this.hardSkills = [];
    for(let skill in list){
      const name = skill;
      const value = list[skill];

      this.hardSkills.push([name, value]);

      setTimeout(() => {
        this.updateHardSkill("hardskill_" + this.id, name, value);
      }, 1);
    }
  }

  _filter(skills: string[], value: string): string[] {
    const filterValue = value.toLowerCase();
    return skills.filter(option => option.toLowerCase().includes(filterValue) && this.hardSkills.findIndex(v => v[0] == option) == -1);
  }

  populate(...args: string[]){
    const map:{[field: string]: any} = {};

    for(const arg of args)
      map[arg] = (<HTMLInputElement>document.getElementById(arg)).value;
    
    return map;
  }

}
