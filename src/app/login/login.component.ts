import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { CurrentUserService } from '../current-user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  onChange(target){

    let valid = false;

    if(target.type === "email"){
      target.value = target.value.slice(0, 64);
      const at = target.value.indexOf("@");
      const dot = target.value.lastIndexOf(".");
      valid = at > 0 && dot > at + 1;
    }
    else if(target.type === "password")
      valid = target.value.length > 0;
      
    if(valid)
      target.classList.remove('invalid');
    else
      target.classList.add('invalid');

  }

  check(id){
    const el = <HTMLInputElement>document.getElementById(id);
    this.onChange(el);
    return !el.classList.contains("invalid");
  }

  login(){
    if(this.check("email") && this.check("password")){
      this.auth.auth.signInWithEmailAndPassword((<HTMLInputElement>document.getElementById("email")).value, (<HTMLInputElement>document.getElementById("password")).value)
        .then(cred => this.loginCred(cred))
        .catch(this.showLoginError);
    }
  }

  loginWithGoogle(){
    let provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('email');

    this.auth.auth.signInWithPopup(provider).then(res => {
      if(res.additionalUserInfo.isNewUser){
        localStorage.setItem("oauthEmail", res.user.email);
      
        if(res.user.displayName != null && res.user.displayName.includes(" ")){
          const split = res.user.displayName.split(" ");
          localStorage.setItem("oauthFirstName", split[0]);
          localStorage.setItem("oauthLastName", split[1]);
        }

        res.user.delete().then();
        this.ngZone.run(() => this.router.navigate(["signup"])).then();
      }
      else
        this.loginCred(res);
      
    }).catch(this.showLoginError);
  }

  async loginCred(cred: firebase.auth.UserCredential){
    localStorage.setItem("uid", cred.user.uid);
    localStorage.setItem("verified", cred.user.emailVerified+"");

    if(cred.user.emailVerified){
      let link = "dashboard";
      
      await this.currentUserService.reload();
      this.currentUserService.runWhenReady(() => this.ngZone.run(() => this.router.navigate(["dashboard"])) );
    }
    else
      this.ngZone.run(() => this.router.navigate(["verify"])).then();
  }

  showLoginError(err){
    const ele = document.getElementById("error");
    ele.style.display = "block";
    let e = "";

    if(err.code === "auth/invalid-email")
      e = "The username or password is invalid."
    else if(err.code === "auth/user-disabled")
      e = "That user account has been disabled."
    else if(err.code === "auth/user-not-found")
      e = "The username or password is invalid."
    else if(err.code === "auth/wrong-password")
      e = "The username or password is invalid."
    else console.log(err);

    ele.innerHTML = "Error: " + e;
  }

  constructor(public router:Router, private auth: AngularFireAuth, private firestore: AngularFirestore, private ngZone: NgZone, private currentUserService: CurrentUserService) { }

  ngOnInit() {
  }

}
