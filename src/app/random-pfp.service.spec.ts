import { TestBed } from '@angular/core/testing';

import { RandomPFPService } from './random-pfp.service';

describe('RandomPFPService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RandomPFPService = TestBed.get(RandomPFPService);
    expect(service).toBeTruthy();
  });
});
