import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InternsCacheService, Intern } from '../interns-cache.service';

export interface Intern {
  uid:string;
  mainInfo:{[field: string]: any};
  skillsInfo:{[field: string]: any};
  university:string;
  pfpPath:string;
}

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  results:Intern[];
  prompts:Intern[];
  term:string;

  constructor(public route: ActivatedRoute, public router: Router, public cache:InternsCacheService) { }

  ngOnInit() {
  }

  go(search:string){
    this.term = search.toLowerCase();
    this.runSearch();
  }

  getOrBlank(item:string){
    return (item !== null && item !== undefined) ? item.toLowerCase() : "";
  }

  includes(item:string){
    return item !== null && item !== undefined && item.toLowerCase().includes(this.term);
  }

  async runSearch(){
    this.router.navigate(['dashboard'], {fragment: 'search'});
    const interns:Intern[] = await this.cache.getInterns();
    this.results = interns.filter(intern => 
      (this.getOrBlank(intern.mainInfo['firstname']) + " " + this.getOrBlank(intern.mainInfo['lastname'])).includes(this.term)
       || this.includes(intern.mainInfo['university'])
    );
  }

}
