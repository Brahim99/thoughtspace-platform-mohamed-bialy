import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

export interface Intern {
  uid:string;
  mainInfo:{[field: string]: any};
  skillsInfo:{[field: string]: any};
  university:string;
  username:string;
  pfpPath:string;
}

@Injectable({
  providedIn: 'root'
})
export class InternsCacheService {

  lastRefresh:number = -1;
  cache:Intern[];

  constructor(private firestore: AngularFirestore) { }

  async getInterns() {
    
    const time = Date.now();
    if(this.cache !== null && (time - this.lastRefresh) <= 5 * 60 * 1000)
      return this.cache;

    this.lastRefresh = time;
    this.cache = [];

    const users = await this.firestore.collection("intern_users").get().toPromise();

    for(let doc of users.docs){
      const uid = doc.id;
      
      const prof = await this.firestore.doc("intern_users/" + uid + "/profile/main").get().toPromise();
      const uni = doc.data()['university'];

      if(doc.data() != undefined && prof.data() != undefined){

        const intern:Intern = {
          uid: uid,
          mainInfo: doc.data(),
          skillsInfo: prof.data(),
          university: uni,
          pfpPath: prof.data()['profilePic'],
          username: doc.data()['username']
        };

        this.cache.push(intern);
      }
    }

    return this.cache;
  }

}
