import { Component, OnInit } from '@angular/core';
import {Label} from "ng2-charts";

@Component({
  selector: 'app-company-engage-webinar',
  templateUrl: './company-engage-webinar.component.html',
  styleUrls: ['./company-engage-webinar.component.css']
})
export class CompanyEngageWebinarComponent implements OnInit {

  constructor() { }

  public ssLabels: Label[] = ["A", "B"];
  public ssColors = [
    { // grey
      backgroundColor: [
        'rgba(255, 163, 58,0.8)',
        'rgba(0,0,0,0)'
      ]
    }
  ];

  ngOnInit() {
  }
}
