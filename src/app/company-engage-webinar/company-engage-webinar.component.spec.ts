import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyEngageWebinarComponent } from './company-engage-webinar.component';

describe('CompanyEngageWebinarComponent', () => {
  let component: CompanyEngageWebinarComponent;
  let fixture: ComponentFixture<CompanyEngageWebinarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyEngageWebinarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyEngageWebinarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
