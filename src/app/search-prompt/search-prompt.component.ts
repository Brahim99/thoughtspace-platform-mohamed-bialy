import { Component, OnInit, Input } from '@angular/core';
import { Intern } from '../interns-cache.service';

@Component({
  selector: 'app-search-prompt',
  templateUrl: './search-prompt.component.html',
  styleUrls: ['./search-prompt.component.css']
})
export class SearchPromptComponent implements OnInit {

  @Input() result: Intern;

  constructor() { }

  ngOnInit() {
  }

}
