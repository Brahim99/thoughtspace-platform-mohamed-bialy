import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicInfoUniversityComponent } from './basic-info-university.component';

describe('BasicInfoUniversityComponent', () => {
  let component: BasicInfoUniversityComponent;
  let fixture: ComponentFixture<BasicInfoUniversityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInfoUniversityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfoUniversityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
