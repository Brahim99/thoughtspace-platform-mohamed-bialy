import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';
import { startWith, map } from 'rxjs/operators';
import { SignupService } from '../signup.service';
import { MatSelectChange } from '@angular/material';
import { isMaster } from 'cluster';

@Component({
  selector: 'app-basic-info-university',
  templateUrl: './basic-info-university.component.html',
  styleUrls: ['./basic-info-university.component.css']
})
export class BasicInfoUniversityComponent implements OnInit {

  // intForm: FormGroup;
  // majorControl = new FormControl();
  // yearControl = new FormControl();

  majors: string[] = ["Administrative and Clerical", "Agricultural Sciences", "Anthropology/Archaeology", "Architecture", "Arts Management", "Biology", "Building and Construction", "Business/Management", "Chemistry", "Communications/Journalism", "Computer Science", "Criminal Justice", "Culinary Arts and Food Service", "Dental", "Design", "Earth and Environmental Sciences", "Economics", "Education", "Engineering", "English", "Film, Video, and Photography", "Finance/Accounting", "Food and Nutrition", "Foreign Languages", "General Studies", "Health Professions", "History", "Humanities", "Information Technology", "International Relations", "Legal Studies", "Mathematics", "Mechanics/Repair", "Medical Assistant/Technician", "Music", "Nursing", "Performing Arts", "Pharmacy", "Philosophy", "Physics", "Political Science", "Protective Services", "Psychology", "Rehabilitation and Therapy", "Religious Studies/Theology", "Trades and Personal Services", "Veterinary Studies", "Visual Arts"];
  // filteredMajors: Observable<string[]>;

  years: string[] = [];
  // filteredYears: Observable<string[]>;

  major: string = null;
  year: string = null;

  constructor(private signupService: SignupService) { }

  ngOnInit() {

    for(let i = 2020; i <= 2023; i++)
      this.years.push(i+"");

    // this.intForm = new FormGroup({'majorControl': this.majorControl, 'yearControl': this.yearControl});

    // this.filteredMajors = this.majorControl.valueChanges
    //   .pipe(
    //     startWith(''),
    //     map(value => this._filter(value))
    //   );

    // this.filteredYears = this.yearControl.valueChanges
    //   .pipe(
    //     startWith(''),
    //     map(value => this._filterY(value))
    //   );
  }

  // _filter(value: string): string[] {
  //   const filterValue = value.toLowerCase();
  //   return this.majors.filter(option => option.toLowerCase().includes(filterValue));
  // }

  // _filterY(value: string): string[] {
  //   const filterValue = value.toLowerCase();
  //   return this.years.filter(option => option.toLowerCase().includes(filterValue));
  // }

  // focus(event){
  //   event.target.value = "";
  //   this.majorControl.setValue('');
  // }

  // focusY(event){
  //   event.target.value = "";
  //   this.yearControl.setValue('');
  // }

  selected(event:MatSelectChange, isMajor){
    if(isMajor){
      this.major = event.value;
      document.getElementById("major").innerHTML = this.major;
    }
    else {
      this.year = event.value;
      document.getElementById("year").innerHTML = this.year;
    }
  }

  next(){

    if(this.major !== null && this.year !== null){
      this.signupService.showNav = true;
      this.signupService.state++;
      if(this.signupService.state > this.signupService.farthest)
      this.signupService.farthest = this.signupService.state;
    }

  }

}
