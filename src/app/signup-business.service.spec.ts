import { TestBed } from '@angular/core/testing';

import { SignupBusinessService } from './signup-business.service';

describe('SignupBusinessService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SignupBusinessService = TestBed.get(SignupBusinessService);
    expect(service).toBeTruthy();
  });
});
