import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import { SignupService } from '../signup.service';

@Component({
  selector: 'app-interests',
  templateUrl: './interests.component.html',
  styleUrls: ['./interests.component.css']
})
export class InterestsComponent implements OnInit {
  
  sectors:string[] = ["Aerospace", "Agriculture", "Automotive", "Aviation", "Banking", "Business Development", "Business Services", "Catering", "Chemical Industries", "Commerce", "Construction", "Dairy", "Defense", "Education", "Electronics", "Energy", "Environment", "Farming", "Financial Services", "Fishing", "Food & Beverage", "Foreign Policy", "Forestry", "Health Services", "Insurance", "Law", "Manufacturing", "Marketing", "Media", "Metal Production", "Mining", "Natural Gas", "News", "Oil and Gas", "Pharmaceutical", "Postal Services", "Public Service", "Real Estate", "Restaurants", "Shipping", "Software Development", "Telecommunications", "Television", "Textiles", "Tourism", "Transport", "UI / UX", "Utilities"];
  selected:string[] = [];

  select(event){
    const val = event.target.innerHTML;
    if(event.target.classList.contains("selected")){
      const ind = this.selected.indexOf(val);
      if(ind > -1)
        this.selected.splice(ind, 1);
      event.target.classList.remove("selected");
    }
    else {
      this.selected.push(val);
      event.target.classList.add("selected");
    }

    document.getElementById("error").style.display = "none";
  }

  constructor(private signupService:SignupService) { }

  ngOnInit() {
  }

  next(){

    if(this.selected.length <= 0){
      document.getElementById("error").style.display = "block";
      return;
    }

    this.signupService.showNav = true;
    this.signupService.state++;
    if(this.signupService.state > this.signupService.farthest)
    this.signupService.farthest = this.signupService.state;

    this.signupService.sectors = this.selected;

  }

}
