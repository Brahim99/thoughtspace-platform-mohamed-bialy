import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {ChangeDetectorRef } from '@angular/core'
var firebase = require('firebase/app');


@Component({
  selector: 'app-taskviewer',
  
  templateUrl: './taskviewer.component.html',
  styleUrls: ['./taskviewer.component.css']
})
export class TaskviewerComponent implements OnInit {

  title: string = ''
  description: string = ''
  dueDate: Date = new Date()
  tasktype: string = ''
  uid:string = ''
  tasks:any[]  = new Array()
  project:string = ''
  

  constructor(private firestore: AngularFirestore,private cd: ChangeDetectorRef) { 
    


  }

  del(task) {
    var docRef = this.firestore.collection('tasks').doc(this.uid)
    //
     docRef.update({tasks: firebase.firestore.FieldValue.arrayRemove(task)})
    if(task.completed == 1)
      task.completed = 0
    else
      task.completed = 1
    docRef.update({tasks: firebase.firestore.FieldValue.arrayUnion(task)}).then(() => {
      //this.tasks = []
      
      //var docRef = this.firestore.collection('tasks').doc(this.uid)
      //docRef.get().subscribe(doc => {
        //if (doc.exists) {
            
            //doc.data()["tasks"].forEach((obj,index) => {
              //this.tasks.push(obj)
            //});
            
  
            
        //} else {
            // doc.data() will be undefined in this case
            //console.log("No such document!");
        //}
    //s});

    })
  }

  ngOnInit() {
    console.log("wws")
    this.tasks = []
    this.uid = firebase.auth().currentUser.uid
    //repalce by code to get the current authinticated user uuid
    var docRef = this.firestore.collection('tasks').doc(this.uid)
    docRef.get().subscribe(doc => {
      if (doc.exists) {
          
          doc.data()["tasks"].forEach((obj,index) => {
            this.tasks.push(obj)
          });
          

          
      } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
      }
  });
  this.cd.markForCheck()

  }
  

}
