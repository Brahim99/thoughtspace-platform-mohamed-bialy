import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyDashboardOverviewComponent } from './company-dashboard-overview.component';

describe('CompanyDashboardOverviewComponent', () => {
  let component: CompanyDashboardOverviewComponent;
  let fixture: ComponentFixture<CompanyDashboardOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyDashboardOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyDashboardOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
