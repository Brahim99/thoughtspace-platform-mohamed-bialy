import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CurrentUserService } from '../current-user.service';

@Component({
  selector: 'app-company-dashboard-overview',
  templateUrl: './company-dashboard-overview.component.html',
  styleUrls: ['./company-dashboard-overview.component.css']
})
export class CompanyDashboardOverviewComponent implements OnInit {

  public rands: {[field: string]: number} = {};

  constructor(public router: Router, public currentUserService:CurrentUserService) { }

  ngOnInit() {
    
  }

  r(name:string){
    if(name in this.rands) return this.rands[name];
    const n = Math.floor(Math.random() * 10 + 1);
    this.rands[name] = n;
    return n;
  }

}
