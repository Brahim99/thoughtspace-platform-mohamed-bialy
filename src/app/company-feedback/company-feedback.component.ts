import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../current-user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-feedback',
  templateUrl: './company-feedback.component.html',
  styleUrls: ['./company-feedback.component.css']
})
export class CompanyFeedbackComponent implements OnInit {

  public allUsers:User[] = null;
  public view:string = "all";
  public filter:string = "all";
  public viewing:User = null;

  constructor(public firestore: AngularFirestore, public router:Router) { }

  async ngOnInit() {

    if(this.allUsers === null){
      const users = await this.firestore.collection("Clariant/results/students").get().toPromise();
      this.allUsers = [];
      for(let doc of users.docs){
        const mainInfo = doc.data();
        const user: User = {
          uid: "",
          username: "",
          pfpPath: "",
          domain: "",
          university: mainInfo["university"],
          isIntern: true,
          mainInfo: mainInfo,
          company: null
        };

        this.allUsers.push(user);
      }
    }

  }

  getShown() : any[] {
    if(this.filter === "all") return this.getFiltered();
    if(this.filter === "software") return this.getFiltered("AJAX", "ASP.NET", "jQuery", "Typescript", "C", "C++", "Java", "PHP", "Hack", "HHVM", "Node.js", "Perl", "Ruby", "C#", "Erlang", "Go", "XHP", "Haskell", "Redis", "MongoDB", "PostgreSQL", "Fortran", "Firebase", "Angular", "Backbone", "Bootstrap", "CodeIgniter", "Django", "Ember", "Express", "Flask", "jQuery", "Laravel ", "Laravel ", "React", "Ruby on Rails", "Spring", "Symphony", "Vue");
    if(this.filter === "business") return this.getFiltered("Jira", "Microsoft Excel", "Project Management", "Adobe Photoshop", "Sketch", "Affinity", "Figma", "Adobe Illustrator", "Adobe XD", "Balsamiq", "Axure", "Flowmapp", "Autosum Function", "AVERAGE", "COUNT", "COUNTA", "IF Functions", "MAX & MIN", "SUM", "SWOT Analysis", "TRIM", "Conditional Formatting", "Heptalysis", "MOST Analysis", "PEST Analysis", "Pivot Tables", "SCRS Analysis", "SWOT Analysis", "VPEC-T Analysis", "Autosum Function", "AVERAGE", "COUNT", "COUNTA", "IF Functions", "MAX & MIN", "SUM", "SWOT Analysis", "TRIM", "CHOOSE (Financial modeling)", "Combination charts - (Dynamic, Rolling chart)", "COUNTIF", "DB - Depreciation", "FV - Future Value", "IF Formulas", "INDEX MATCH", "INTRATE -Interst rate", "LEN", "PMT and IPMT", "SUMIF", "Text-Formulas", "VLOOKUP", "XNPV and XIRR", "YIELD");
    if(this.filter === "marketing") return this.getFiltered("Adobe Premiere Pro CC", "Ahrefs", "Canva", "Chrome Eyedropper", "Crazy Egg", "Creative Cloud", "Drift (AI Chat)", "Feathr", "Gainsight", "Google Analytics", "Google console", "Google Pagespeed Insights", "Google Tag Manager", "Google URL Builder", "Grammarly", "GTMetrix (site loading)", "Hootsuite", "Hubspot", "inDesign", "Intercom", "inVision", "Lastpass", "LSI Graph", "Mail Chimp", "Optimizely", "Owler", "Piktochart", "Pixlr", "Premier", "Principle", "Quicksprout", "Salesforce", "Screaming Frog", "Siftrock ", "Subscribers", "Webflow");
    return [];
  }

  getFiltered(...skills) : any[] {
    if(this.allUsers === null) return [];

    const filtered:any[] = [];
    for(let user of this.allUsers){
      const relevant = [];
      for(let skill of user.mainInfo['hardSkills']){
        if(skills.includes(skill) || skills.length == 0)
          relevant.push(skill);
      }
      if(relevant.length > 0)
        filtered.push([user, relevant]);
    }

    return filtered;
  }

}
