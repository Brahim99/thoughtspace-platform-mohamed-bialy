import { Component, OnInit } from '@angular/core';
import { CurrentUserService, Project } from '../current-user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-workspace',
  templateUrl: './dashboard-workspace.component.html',
  styleUrls: ['./dashboard-workspace.component.css']
})
export class DashboardWorkspaceComponent implements OnInit {

  constructor(public currentUserService: CurrentUserService, public router: Router, public routed: ActivatedRoute) { }

  project: Project;

  ngOnInit() {
    this.routed.url.subscribe(url => {
      if(url.length > 2 && url[1].path == "project"){
        const proj = url[2].path;
        const project = this.currentUserService.projects.find(p => p.name == proj);
        if(project == null || project == undefined || !project.accepted)
          this.router.navigate(["dashboard"]);
        else {
          this.project = project;
          console.log(this.project)
        }
        return;
      }
    });
  }

  hideMenu(){
    document.getElementById("actions-menu").style.display = "none";
  }

  toggleMenu(){
    const e = document.getElementById("actions-menu");
    if(e.style.display === "block")
      e.style.display = "none";
    else
      e.style.display = "block";
  }
}
