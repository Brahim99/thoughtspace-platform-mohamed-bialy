import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentEvaluateComponent } from './student-evaluate.component';

describe('StudentEvaluateComponent', () => {
  let component: StudentEvaluateComponent;
  let fixture: ComponentFixture<StudentEvaluateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentEvaluateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentEvaluateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
