import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CurrentUserService, Project } from '../current-user.service';
import { MatSliderChange } from '@angular/material';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-student-evaluate',
  templateUrl: './student-evaluate.component.html',
  styleUrls: ['./student-evaluate.component.css']
})
export class StudentEvaluateComponent implements OnInit {

  viewing: Project;

  constructor(public routed: ActivatedRoute, public router: Router, public firestore: AngularFirestore, public currentUserService: CurrentUserService) { }

  ngOnInit() {

    window.scrollTo(0, 0);
    
    this.routed.url.subscribe(url => {
      if(url != null && url != undefined && url.length >2 && url[1].path == "student"){
        console.log(url);
        const name = url[2].path;
        
        this.currentUserService.runWhenReady(() => {
          const project = this.currentUserService.currentUser.company.projects[name];
          if(project == undefined)
            this.router.navigate(["dashboard"]);
          else
            this.load(project);
        });
      }
    });

  }

  onSlide(event: MatSliderChange){
    const element = <HTMLElement> event.source._elementRef.nativeElement;
    const display = <HTMLElement> (element.parentElement.childNodes[0].childNodes[1]);
    display.innerHTML = event.value + "";
  }

  load(project: Project){
    this.viewing = project;
  }

  cancel(){
    this.router.navigate(["dashboard"]);
  }

  submit(){
    this.cancel();
  }

}
