import { Component, OnInit } from '@angular/core';
import {Label} from "ng2-charts";

@Component({
  selector: 'app-company-engage-challenge',
  templateUrl: './company-engage-challenge.component.html',
  styleUrls: ['./company-engage-challenge.component.css']
})
export class CompanyEngageChallengeComponent implements OnInit {

  constructor() { }

  public postOpen:boolean = false;
  public state:number = 0;

  public ssLabels: Label[] = ["A", "B"];
  public ssColors = [
    { // grey
      backgroundColor: [
        'rgba(152, 112, 251,0.8)',
        'rgba(0,0,0,0)'
      ]
    }
  ];

  ngOnInit() {
  }

}
