import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyEngageChallengeComponent } from './company-engage-challenge.component';

describe('CompanyEngageChallengeComponent', () => {
  let component: CompanyEngageChallengeComponent;
  let fixture: ComponentFixture<CompanyEngageChallengeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyEngageChallengeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyEngageChallengeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
