import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SignupComponent } from './signup/signup.component';
import { DashboardGuardService } from './dashboardguard.service';
import { SignupGuardService } from './signupguard.service';
import { LoginComponent } from './login/login.component';
import { VerifyComponent } from './verify/verify.component';
import { VerifyGuardService } from './verify-guard.service';
import { PasswordResetComponent } from './password-reset/password-reset.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { SignupBusinessComponent } from './signup-business/signup-business.component';
import { CompanyPageComponent } from './company-page/company-page.component';
import { SearchComponent } from './search/search.component';
import { ProfileComponent } from './profile/profile.component';
import { DashboardEngageComponent } from './dashboard-engage/dashboard-engage.component';
import { DashboardRewardsComponent } from './dashboard-rewards/dashboard-rewards.component';
import { TaskViewComponent } from './task-view/task-view.component';
import { FormStudentComponent } from './form-student/form-student.component';
import { TaskComponent } from './task/task.component';
import { TaskviewerComponent } from './taskviewer/taskviewer.component';
import {RlpDemoComponent} from './rlp-demo/rlp-demo.component';
import {CompanyEngageChallengeComponent} from "./company-engage-challenge/company-engage-challenge.component";
import {CompanyEngageCourseComponent} from "./company-engage-course/company-engage-course.component";
import {CompanyEngageSurveyComponent} from "./company-engage-survey/company-engage-survey.component";
import {CompanyEngageWebinarComponent} from "./company-engage-webinar/company-engage-webinar.component";

const routes: Routes = [
  {path: "student-form", component: FormStudentComponent},

  {path: "RLP", component: RlpDemoComponent},

  {path: "dashboard", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/create", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/edit", redirectTo: "dashboard", pathMatch: 'full'},
  {path: "dashboard/edit/:project", component: DashboardComponent, canActivate: [DashboardGuardService]},

  {path: "dashboard/project", redirectTo: "dashboard", pathMatch: 'full'},
  {path: "dashboard/project/:project", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/survey/:project", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/evaluate/:project/:intern", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/projects", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/practice", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/analytics", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/talent", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/evaluate", component: DashboardComponent, canActivate: [DashboardGuardService]},
  {path: "dashboard/ballofyarn", component: DashboardComponent, canActivate: [DashboardGuardService]},

  {path: "dashboard/student/:project", component: DashboardComponent, canActivate: [DashboardGuardService]},

  {path: 'engage', component: DashboardEngageComponent, canActivate: [DashboardGuardService]},
  {path: 'engage/challenges', component: DashboardEngageComponent, canActivate: [DashboardGuardService]},
  {path: 'engage/courses', component: DashboardEngageComponent, canActivate: [DashboardGuardService]},
  {path: 'engage/surveys', component: DashboardEngageComponent, canActivate: [DashboardGuardService]},
  {path: 'engage/webinars', component: DashboardEngageComponent, canActivate: [DashboardGuardService]},

  {path: 'rewards', component: DashboardRewardsComponent, canActivate: [DashboardGuardService]},
  {path: 'tasks', component: TaskViewComponent, canActivate: [DashboardGuardService]},
  {path: "signup", component: SignupComponent, canActivate: [SignupGuardService]},
  {path: "signupBusiness", component: SignupBusinessComponent, canActivate: [SignupGuardService]},
  {path: "login", component: LoginComponent, canActivate: [SignupGuardService]},
  {path: "verify", component: VerifyComponent, canActivate: [VerifyGuardService]},
  {path: "passwordreset", component: PasswordResetComponent},
  {path: "home", component: LandingPageComponent},
  {path: 'OneTimeEmailVerification/:uid/:token', component: EmailVerificationComponent},
  {path: 'companies/:company', component: CompanyPageComponent},
  {path: 'search/:search', component: SearchComponent},
  {path: 'profiles/:user', component: ProfileComponent},
  {path: "", redirectTo: "/home", pathMatch: 'full'},
  {path: 'dashboard/project/:projectname/addtask',  pathMatch: 'full',component: TaskComponent,canActivate: [DashboardGuardService]},
  {path: 'dashboard/project/:projectname/viewtasks', pathMatch: 'full', component: TaskviewerComponent,canActivate: [DashboardGuardService]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
