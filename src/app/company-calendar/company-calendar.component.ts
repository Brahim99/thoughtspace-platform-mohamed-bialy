import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company-calendar',
  templateUrl: './company-calendar.component.html',
  styleUrls: ['./company-calendar.component.css']
})
export class CompanyCalendarComponent implements OnInit {

  public months:string[] = ["January", "February", 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  public month: number;
  public day: number;
  public daysInYear:number;
  public year: number;

  constructor() { }

  ngOnInit() {
    const d = new Date();
    this.day = d.getDate();
    this.month = d.getMonth();
    this.year = d.getFullYear();
  }

  daysInMonth(){
    return new Date(this.year, this.month + 1, 0).getDate();
  }

  nextMonth(){
    this.month++;
    if(this.month >= 12){
      this.month = 0;
      this.year++;
    }
    const maxDay = this.daysInMonth();
    if(this.day > maxDay)
      this.day = maxDay;
  }

  prevMonth(){
    this.month--;
    if(this.month < 0){
      this.month = 11;
      this.year--;
    }
    const maxDay = this.daysInMonth();
    if(this.day > maxDay)
      this.day = maxDay;
  }

}
