import { TestBed } from '@angular/core/testing';

import { InternsCacheService } from './interns-cache.service';

describe('InternsCacheService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InternsCacheService = TestBed.get(InternsCacheService);
    expect(service).toBeTruthy();
  });
});
