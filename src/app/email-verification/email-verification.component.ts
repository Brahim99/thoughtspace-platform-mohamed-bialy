import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})
export class EmailVerificationComponent implements OnInit {

  constructor(public route: ActivatedRoute, public http:HttpClient, public auth: AngularFireAuth, public router:Router) { }

  ngOnInit() {
    const uid = this.route.snapshot.paramMap.get("uid");
    const token = this.route.snapshot.paramMap.get("token");

    this.http.post("https://cors-anywhere.herokuapp.com/https://us-central1-devproduct.cloudfunctions.net/authfunctions-verifyUser_fromverificationemail", {
      "uid": uid,
      "gvt": token
    }).toPromise().then(() => {
      this.router.navigate(['dashboard']);
      localStorage.setItem("verified", "true");
    }).catch(() => {
      document.getElementById("error").style.display = "block";
    });
  }

}
