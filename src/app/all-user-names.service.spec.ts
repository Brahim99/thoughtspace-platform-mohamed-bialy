import { TestBed } from '@angular/core/testing';

import { AllUserNamesService } from './all-user-names.service';

describe('AllUserNamesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AllUserNamesService = TestBed.get(AllUserNamesService);
    expect(service).toBeTruthy();
  });
});
