import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardCompleteprofileComponent } from './dashboard-completeprofile.component';

describe('DashboardCompleteprofileComponent', () => {
  let component: DashboardCompleteprofileComponent;
  let fixture: ComponentFixture<DashboardCompleteprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardCompleteprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardCompleteprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
