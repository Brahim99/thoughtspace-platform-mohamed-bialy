import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

export interface Task {
    name: string;
    time: number;
}

@Injectable({
  providedIn: 'root'
})
export class TaskTimerService {

  tasks:{[field: string]: Task} = {};

  constructor(private firestore: AngularFirestore) { 
    setInterval(() => {
      for(let task in this.tasks)
        this.tasks[task].time += 0.01;
    }, 10);
  }

  addTask(name: string){
    const task:Task = {
      name: name,
      time: 0
    };

    this.tasks[name] = task;

    return task;
  }

  completeTask(name: string){
    
    if(!this.tasks[name]) return -1;
      
    const time = this.tasks[name].time;
    
    this.firestore.doc("intern_users" + "/" + localStorage.getItem("uid") + "/" + "tasks/" + name)
      .set({name: name, time: time, date: Date.now()}).then();
  
    delete this.tasks[name];
    return time;

  }

  deleteTask(name: string){
    if(this.tasks[name])
      delete this.tasks[name];
  }

  getTime(name: string){
    if(!this.tasks[name]) return -1;
    return this.tasks[name].time;
  }

}
