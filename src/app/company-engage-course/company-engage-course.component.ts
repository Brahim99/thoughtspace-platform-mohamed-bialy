import { Component, OnInit } from '@angular/core';
import {Label} from "ng2-charts";

@Component({
  selector: 'app-company-engage-course',
  templateUrl: './company-engage-course.component.html',
  styleUrls: ['./company-engage-course.component.css']
})
export class CompanyEngageCourseComponent implements OnInit {

  constructor() { }

  public ssLabels: Label[] = ["A", "B"];
  public ssColors = [
    { // grey
      backgroundColor: [
        'rgba(46, 223, 134,0.8)',
        'rgba(0,0,0,0)'
      ]
    }
  ];

  ngOnInit() {
  }

}
