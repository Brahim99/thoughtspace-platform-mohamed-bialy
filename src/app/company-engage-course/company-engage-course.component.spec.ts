import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyEngageCourseComponent } from './company-engage-course.component';

describe('CompanyEngageCourseComponent', () => {
  let component: CompanyEngageCourseComponent;
  let fixture: ComponentFixture<CompanyEngageCourseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyEngageCourseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyEngageCourseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
