import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BasicInfoBusinessComponent } from './basic-info-business.component';

describe('BasicInfoBusinessComponent', () => {
  let component: BasicInfoBusinessComponent;
  let fixture: ComponentFixture<BasicInfoBusinessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BasicInfoBusinessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BasicInfoBusinessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
