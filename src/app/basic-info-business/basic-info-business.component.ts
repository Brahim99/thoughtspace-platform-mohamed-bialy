import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { SignupBusinessService } from '../signup-business.service';
import { VerifyService } from '../verify.service';
import { RandomPFPService } from '../random-pfp.service';

@Component({
  selector: 'app-basic-info-business',
  templateUrl: './basic-info-business.component.html',
  styleUrls: ['./basic-info-business.component.css']
})
export class BasicInfoBusinessComponent implements OnInit {

  subscription: any;
  constructor(public signupService:SignupBusinessService, public router:Router, private auth: AngularFireAuth, private firestore: AngularFirestore, private verifier:VerifyService, private randomPFP: RandomPFPService) { }

  registeredCompanyName:string = null;
  companyName:string = "Company Name";

  toggleCB(){
    const ele = <HTMLInputElement>document.getElementById("checkbox");
    ele.checked = !ele.checked;
  }
  toggleCB2(){
    const ele = <HTMLInputElement>document.getElementById("allowMessage");
    ele.checked = !ele.checked;
  }
  toggleCB3(){
    const ele = <HTMLInputElement>document.getElementById("autoApprove");
    ele.checked = !ele.checked;
  }
  toggleCB4(){
    const ele = <HTMLInputElement>document.getElementById("requestJoin");
    ele.checked = !ele.checked;
  }

  onChange(target){

    let valid = false;

    if(target.type === "email"){
      target.value = target.value.slice(0, 64);
      const at = target.value.indexOf("@");
      const dot = target.value.lastIndexOf(".");
      valid = at > 0 && dot > at + 1;

      if(valid)
        document.getElementById("errEmail2").style.display = "none";
      else
        document.getElementById("errEmail2").style.display = "block";

      if(valid){
        this.auth.auth.fetchSignInMethodsForEmail(target.value).then(methods => {
          if(methods.length != 0){
            target.classList.add('invalid');
            this.signupService.state = 0;
            document.getElementById("errEmail").style.display = "block";
          }
          else {
            document.getElementById("errEmail").style.display = "none";

            const start = target.value.lastIndexOf("@") + 1;
            const end = target.value.lastIndexOf(".");
            const domain = target.value.substring(start, end).toLowerCase();
            this.companyName = domain;
            if(this.registeredCompanyName != domain){
              this.firestore.collection("business_page").doc(domain).get().toPromise().then(doc => {
                if(doc.exists)
                  this.registeredCompanyName = domain;
                else this.registeredCompanyName = null;
              });
            }
          }
       });
      }
    }
    else if(target.type === "tel")
      valid = this.format(target, target.value);
    else if(target.type === "checkbox")
      valid = target.checked;
    else if(target.type === "password"){
      const box = <HTMLInputElement>document.getElementById("password");
      if(target.id === "confirmPassword"){
        valid = target.value === box.value;
        const err = document.getElementById("errConfirm");
        if(valid)
          err.style.display = "none";
        else
          err.style.display = "block";
      }
      else {
        const err = document.getElementById("errPass");
        valid = target.value.length >= 6;
        if(valid)
          err.style.display = "none";
        else
          err.style.display = "block";
      }
    }
    else if(target.id === "zipcode"){
      target.value = target.value.replace(/[^0-9]/g, '').slice(0, 5);
      valid = target.value.length === 5;
    }
    else if(this.signupService.state == 0){
      target.value = target.value.replace(/[^a-z\-A-Z]/g, '').slice(0, 32);
      valid = target.value.length > 0;
    }
    else if(this.signupService.state == 2){
      target.value = target.value.replace(/[^a-z\-A-Z\_0-9]/g, '').slice(0, 32);
      valid = target.value.length > 0;
    }
    else {
      valid = target.value.length > 0;
      target.value = target.value.slice(0, 64);
    }

    if(valid)
      target.classList.remove('invalid');
    else
      target.classList.add('invalid');

  }

  format(target, tel:string){
    let raw = tel.replace(/[^0-9]/g, '');
    let good:boolean = raw.length >= 10;

    let formatted = "";

    if(raw.length > 0){
      formatted = "(";
      if(raw.length > 3){
        formatted += raw.slice(0, 3) + ") ";

        if(raw.length > 6)
          formatted += raw.slice(3, 6) + "-" + raw.slice(6, 10);
        else
          formatted += raw.slice(3);
      }
      else
        formatted += raw;
    }

    target.value = formatted;

    return good;
  }

  check(){
    if(this.signupService.state == 1 && this.registeredCompanyName !== null)
      return true;

    var list = document.getElementsByClassName("s-" + this.signupService.state);
    let valid = true;

    for(let i = 0; i < list.length; i++){
      const el = <HTMLInputElement>list[i];
      this.onChange(el);
      if(el.classList.contains("invalid"))
        valid = false;
    }
    return valid;
  }

  next(){

    if(this.check()){
      this.signupService.showNav = true;
      this.signupService.state++;
      if(this.signupService.state > this.signupService.farthest)
      this.signupService.farthest = this.signupService.state;
    }
  }

  ngOnInit() {
    this.subscription = this.signupService.doJump.subscribe(item => {
      this.check();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  populate(...args: string[]){
    const map:{[field: string]: any} = {};

    for(const arg of args)
      map[arg] = (<HTMLInputElement>document.getElementById(arg)).value;

    return map;
  }

  pushing:boolean = false;

  finish(){

    if(this.pushing)
      return;

    this.pushing = true;

    const email = (<HTMLInputElement>document.getElementById("email")).value;
    const pass = (<HTMLInputElement>document.getElementById("password")).value;
    this.auth.auth.createUserWithEmailAndPassword(email, pass).then(async(cred) => {

      const uid = cred.user.uid;
      localStorage.setItem("uid", uid);
      localStorage.setItem("intern", "false");

      const map:{[field: string]: any} = this.populate("email", "firstname", "lastname", "username", "company", "phone", "street", "city", "state", "country", "zipcode");
      map["typeFlag"] = "b";
      map["uid"] = uid;
      map["phone"] = map["phone"].replace(/[^0-9]/g, '');
      map["dateCreated"] = Date.now();
      map["dateToDelete"] = (Date.now() + 30 * 24 * 60 * 60);

      const start = email.lastIndexOf("@") + 1;
      const end = email.lastIndexOf(".");
      map["domain"] = email.substring(start, end).toLowerCase();

      localStorage.setItem("domain", map["domain"]);

      const desc = "Hello! My name is " + map['firstname'] + ". You can email me at " + email + ", or call me directly at " + this.formatPhone(map['phone']) + ".";

      this.firestore.doc("pending_users/" + uid).set(map);
      this.firestore.doc("pending_users/" + uid + "/profile/main").set(
        {
          "description": desc,
          "profilePic": this.randomPFP.getLink()
        }
      );

      this.verifier.sendVerification(this.auth.auth.currentUser.uid);

      this.router.navigate(["verify"]);

    }).catch(err => {
      this.pushing = false;
    });
  }

  formatPhone(tel:string){
    let raw = tel.replace(/[^0-9]/g, '');

    let formatted = "";

    if(raw.length > 0){
      formatted = "(";
      if(raw.length > 3){
        formatted += raw.slice(0, 3) + ") ";

        if(raw.length > 6)
          formatted += raw.slice(3, 6) + "-" + raw.slice(6, 10);
        else
          formatted += raw.slice(3);
      }
      else
        formatted += raw;
    }

    return formatted;
  }
}
