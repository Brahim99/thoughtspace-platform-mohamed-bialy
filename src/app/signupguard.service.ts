import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class SignupGuardService implements CanActivate {

  constructor(public auth:AngularFireAuth, public router:Router) { }

  canActivate(): boolean {
    if (localStorage.getItem("uid") !== null) {
      if(localStorage.getItem("verified") !== "true")
        this.router.navigate(['verify']);
      else
        this.router.navigate(['dashboard']);
      return false;
    }
    return true;
  }

}
