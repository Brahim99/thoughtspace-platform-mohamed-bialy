import { Component, OnInit } from '@angular/core';
import { CompanyDataService } from '../company-data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-company-page',
  templateUrl: './company-page.component.html',
  styleUrls: ['./company-page.component.css']
})
export class CompanyPageComponent implements OnInit {

  constructor(public data: CompanyDataService, private route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    this.data.reload(this.route.snapshot.paramMap.get("company"));
  }

}
