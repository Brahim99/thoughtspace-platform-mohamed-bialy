import { Component, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { CompanyCreateprojectComponent } from '../company-createproject/company-createproject.component';
import { CurrentUserService, Project } from '../current-user.service';
import { CompanyViewprojectComponent } from '../company-viewproject/company-viewproject.component';
import { FeedbackComponent } from '../feedback/feedback.component';

@Component({
  selector: 'app-company-dashboard',
  templateUrl: './company-dashboard.component.html',
  styleUrls: ['./company-dashboard.component.css']
})
export class CompanyDashboardComponent implements OnInit {

  view: string = "";
  @ViewChild("createProject") createproject: CompanyCreateprojectComponent;
  @ViewChild("viewProject") viewproject: CompanyViewprojectComponent;
  @ViewChild("feedbackComponent") feedbackComponent: FeedbackComponent;

  constructor(public router:Router, public firestore: AngularFirestore, public routed: ActivatedRoute, public currentUserService: CurrentUserService) { }

  async ngOnInit() {

    const frag = this.routed.snapshot.fragment;
    if(frag !== null && frag !== undefined)
      this.view = frag;

    this.routed.url.subscribe(url => {
      if(url !== null && url !== undefined && url.length > 1){
        this.view = url[1].path;
        if((this.view == "edit" || this.view == "project") && url.length <= 2)
          this.view = "";
      }
      else
        this.view = "";
    });

    if(!this.currentUserService.isReady){
      await this.currentUserService.reload();
    }
    
  }

  viewProj(project: Project){
    this.viewproject.reset();
    this.router.navigate(['dashboard/project/'+project.name]);
    this.viewproject.load(project);
  }

  edit(project: Project){
    this.createproject.reset();
    this.router.navigate(['dashboard/edit/'+project.name]);
    this.createproject.load(project);
  }

  delete(project: Project){
    const collection = this.firestore.collection("business_page").doc(this.currentUserService.currentUser.domain).collection("projects");
    collection.doc(project.name).delete();
    delete this.currentUserService.currentUser.company.projects[project.name];
    this.router.navigate(["dashboard"]);
  }

  create(){
    this.createproject.reset();
    this.router.navigate(['dashboard/create']);
  }

  hideEditProject(): boolean {
    return this.view != 'create' && this.view != "edit";
  }

  hideViewProject(): boolean {
    return this.view != "project";
  }

}
