import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardInternshipsComponent } from './dashboard-internships.component';

describe('DashboardInternshipsComponent', () => {
  let component: DashboardInternshipsComponent;
  let fixture: ComponentFixture<DashboardInternshipsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardInternshipsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardInternshipsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
