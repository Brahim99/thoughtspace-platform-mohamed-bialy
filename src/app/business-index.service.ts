import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BusinessIndexService {

  lastRefresh:number;
  cache:{[field: string]: string};

  constructor(private firestore: AngularFirestore) { 
    this.cache = JSON.parse(localStorage.getItem("businessIndexCache"));
    this.lastRefresh = +localStorage.getItem("businessIndexCacheLastRef");
  }

  async getIndex() {
    
    const time = Date.now();
    if(this.cache !== null && (time - this.lastRefresh) <= 24 * 60 * 60 * 1000)
      return this.cache;

    this.lastRefresh = time;
    this.cache = {};

    const index = await this.firestore.collection("BusinessDomainSearchIndex").get().toPromise();
    for(let doc of index.docs)
      this.cache[doc.id] = doc.data()['company'];
    
    localStorage.setItem("businessIndexCache", JSON.stringify(this.cache));
    localStorage.setItem("businessIndexCacheLastRef", this.lastRefresh+"");

    return this.cache;
  }

}
