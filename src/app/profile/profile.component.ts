import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InternsCacheService } from '../interns-cache.service';
import { Intern } from '../search/search.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  username: string;
  intern: Intern = null;
  done: boolean = false;

  constructor(public router: Router, public routed: ActivatedRoute, public internCache: InternsCacheService) { }

  ngOnInit() {

    this.done = false;

    this.routed.params.subscribe(params => {
      this.switch(params['user']);
    });

    this.username = this.routed.snapshot.paramMap.get("user");
    this.load().catch(err => {

    });

  }

  async load(){
    const uL = this.username.toLowerCase();
    const interns = await this.internCache.getInterns();
    for(const intern of interns){
      if(intern.username.toLowerCase() === uL){
        this.intern = intern;
        break;
      }
    }
    this.done = true;
    
    document.getElementById("notfound").style.display = this.intern === null ? "block" : "none";
  }

  switch(username:string){
    this.done = false;
    this.intern = null;
    this.username = username;
    this.load().catch(err => {

    });
  }

}
