import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { VerifyService } from '../verify.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.css']
})
export class VerifyComponent implements OnInit {

  constructor(public auth:AngularFireAuth, public router:Router, public verifier:VerifyService) { }

  last = 0;
  timer:NodeJS.Timer;

  signout(){
    localStorage.clear();
    this.auth.auth.signOut();
    this.router.navigate(["login"]);
  }

  refresh(){
    this.auth.auth.onAuthStateChanged(user => {
      if(user){
        user.getIdToken(true).then(async(o) => {
          user.reload();
          localStorage.setItem("verified", user.emailVerified+"");
          if(user.emailVerified){
            this.router.navigate(["dashboard"]);
          }
        });
      }
      else
        this.router.navigate(["login"]);
    });
  }

  resend(){

    this.refresh();

    if(this.auth.auth.currentUser === null){
      this.router.navigate(["login"]);
      return;
    }

    if(Date.now() - this.last <= 30 * 1000){
      document.getElementById("error").style.display = "block";
      return;
    }

    this.last = Date.now();

    document.getElementById("error").style.display = "none";
    
    document.getElementById("sent").style.display = "none";
    document.getElementById("sending").style.display = "block";
    
    // this.auth.auth.currentUser.sendEmailVerification().then(() => {
    this.verifier.sendVerification(this.auth.auth.currentUser.uid).then(() => {
      document.getElementById("sent").style.display = "block";
      document.getElementById("sending").style.display = "none";
    });
  }

  ngOnInit() {
    this.timer = setInterval(() => this.refresh(), 2000);
  }

  ngOnDestroy(){
    clearInterval(this.timer);
  }

}
