import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RlpDemoComponent } from './rlp-demo.component';

describe('RlpDemoComponent', () => {
  let component: RlpDemoComponent;
  let fixture: ComponentFixture<RlpDemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RlpDemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RlpDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
