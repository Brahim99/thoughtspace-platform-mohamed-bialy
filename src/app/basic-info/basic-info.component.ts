import { Component, OnInit } from '@angular/core';
import { SignupService } from '../signup.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormGroup, FormControl } from '@angular/forms';
import { FinishStudentSignupService } from '../finish-student-signup.service';

@Component({
  selector: 'app-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['./basic-info.component.css']
})
export class BasicInfoComponent implements OnInit {

  subscription: any;
  constructor(public signupService:SignupService, public router:Router, private auth: AngularFireAuth, private firestore: AngularFirestore, private finishSignup: FinishStudentSignupService) { }

  toggleCB(){
    const ele = <HTMLInputElement>document.getElementById("checkbox");
    ele.checked = !ele.checked;
  }

  onChange(target){

    let valid = false;

    if(target.type === "email"){
      target.value = target.value.slice(0, 64);
      // valid = target.value.includes("@") && target.value.endsWith(".edu");
      const at = target.value.indexOf("@");
      const dot = target.value.lastIndexOf(".");
      valid = at > 0 && dot > at + 1;

      if(valid)
        document.getElementById("errEmail2").style.display = "none";
      else
        document.getElementById("errEmail2").style.display = "block";

      if(valid){
        this.auth.auth.fetchSignInMethodsForEmail(target.value).then(methods => {
          if(methods.length != 0){
            target.classList.add('invalid');
            this.signupService.state = 0;
            document.getElementById("errEmail").style.display = "block";
          }
          else
            document.getElementById("errEmail").style.display = "none";
       }).catch(err => {
          target.classList.add('invalid');
          this.signupService.state = 0;
          document.getElementById("errEmail").style.display = "block";
       });
      }
    }
    else if(target.type === "tel")
      valid = this.format(target, target.value);
    else if(target.type === "checkbox")
      valid = target.checked;
    else if(target.type === "password"){
      const box = <HTMLInputElement>document.getElementById("password");
      if(target.id === "confirmPassword"){
        valid = target.value === box.value;
        const err = document.getElementById("errConfirm");
        if(valid)
          err.style.display = "none";
        else 
          err.style.display = "block";
      }
      else {
        const err = document.getElementById("errPass");
        valid = target.value.length >= 6;
        if(valid)
          err.style.display = "none";
        else 
          err.style.display = "block";
      }
    }
    else if(target.id === "zipcode"){
      target.value = target.value.replace(/[^0-9]/g, '').slice(0, 5);
      valid = target.value.length === 5;
    }
    else if(target.id === "username"){
      target.value = target.value.replace(/[^a-z\-A-Z\_0-9]/g, '').slice(0, 32);
      valid = target.value.length > 0;

      if(valid){
        this.firestore.collection("users").ref.where("username", "==", target.value).get().then(collection => {
          if(collection.docs.length > 0){
            target.classList.add('invalid');
            this.signupService.state = 1;
            document.getElementById("errUser").style.display = "block";
          }
          else {
            target.classList.remove('invalid');
            document.getElementById("errUser").style.display = "none";
          }
        });
      }
    }
    else if(this.signupService.state == 0){
      target.value = target.value.replace(/[^a-z\-A-Z]/g, '').slice(0, 32);
      valid = target.value.length > 0;
    }
    else {
      valid = target.value.length > 0;
      target.value = target.value.slice(0, 64);
    }

    if(valid)
      target.classList.remove('invalid');
    else
      target.classList.add('invalid');

  }

  format(target, tel:string){
    let raw = tel.replace(/[^0-9]/g, '');
    let good:boolean = raw.length >= 10;
    
    let formatted = "";

    if(raw.length > 0){
      formatted = "(";
      if(raw.length > 3){
        formatted += raw.slice(0, 3) + ") ";

        if(raw.length > 6)
          formatted += raw.slice(3, 6) + "-" + raw.slice(6, 10);
        else
          formatted += raw.slice(3);
      }
      else
        formatted += raw;
    }

    target.value = formatted;

    return good;
  }

  check(){
    var list = document.getElementsByClassName("s-" + this.signupService.state);
    let valid = true;

    for(let i = 0; i < list.length; i++){
      const el = <HTMLInputElement>list[i];
      this.onChange(el);
      if(el.classList.contains("invalid"))
        valid = false;
    }
    return valid;
  }

  next(){

    if(this.check()){

      // if(this.signupService.state === 0)
      //   this.setUniversity((<HTMLInputElement> document.getElementById("email")).value);

      this.signupService.showNav = true;
      this.signupService.state++;
      if(this.signupService.state > this.signupService.farthest)
      this.signupService.farthest = this.signupService.state;
    }
  }

  ngOnInit() {
    this.subscription = this.signupService.doJump.subscribe(item => {
      this.check();
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  finish(){
    this.finishSignup.finish();
  }

  ngAfterViewInit(){
    const email = localStorage.getItem("oauthEmail");
    const first = localStorage.getItem("oauthFirstName");
    const last = localStorage.getItem("oauthLastName");

    if(email != null)
      (<HTMLInputElement> document.getElementById("email")).value = email;
    if(first != null)
      (<HTMLInputElement> document.getElementById("firstname")).value = first;
    if(last != null)
      (<HTMLInputElement> document.getElementById("lastname")).value = last;

    localStorage.removeItem("oauthEmail");
    localStorage.removeItem("oauthFirstName");
    localStorage.removeItem("oauthLastName");
  }

  // async setUniversity(email:string){
  //   const start = email.indexOf("@") + 1;
  //   if(start <= 0) return;
  //   const end = email.indexOf(".edu");
  //   if(end <= 0) return;
  //   let id = email.substring(start, end).toLowerCase();

  //   const notherDot = id.lastIndexOf(".");
  //   if(notherDot > -1)
  //     id = id.substring(notherDot + 1);

  //   const matches = await this.firestore.collection("UniversityDomainSearchIndex").ref.where("domain", "==", id).get();
  //   if(matches.docs.length > 0){
  //     document.getElementById("universityName").innerHTML = matches.docs[0].data()['university'];
  //   }
  // }
}