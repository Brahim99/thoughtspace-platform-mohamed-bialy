runtime: python27
api_version: 1
threadsafe: true

handlers:
- url: /.*
  static_files: dist/ThoughtSpacev1/index.html
  upload: dist/ThoughtSpacev1/index.html
- url: /
  static_dir: dist/ThoughtSpacev1

skip_files:
  - e2e/
  - node_modules/
  - src/
  - ^(.*/)?\..*$
  - ^(.*/)?.*\.json$
  - ^(.*/)?.*\.md$
  - ^(.*/)?.*\.yaml$
  - ^LICENSE
